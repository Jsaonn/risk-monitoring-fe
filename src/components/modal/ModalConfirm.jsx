// ** React Imports 
import React from 'react';

// ** MUI Components 
import {
    Box,
    Button,
    Dialog,
    Grid,
    Typography,
} from "@mui/material"

export const ModalConfirm = ({ open, onClose, data, onDelete }) => {

    return(
        <Dialog open={open} onClose={onClose}>
            <Box
                sx={{ 
                    minWidth: { md: 600, xs: '100%' },
                    px: 10,
                    py: '50px',
                 }}
            >
                <Typography variant="body2" color="black" align="center">Are you sure want to delete "{data.name}"?</Typography>
                <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', gap: 2, mt: 2 }}>
                    <Button variant="outlined" color="error" onClick={onClose}>CANCEL</Button>
                    <Button variant="contained" onClick={()=>onDelete(data.id)}>CONFIRM</Button>
                </Box>
            </Box>
        </Dialog>
    )
}