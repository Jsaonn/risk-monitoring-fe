// ** React Imports 
import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { apiGetAllKeteranganData, apiDeleteKeterangan } from 'lib/axios';

// ** MUI Components 
import { Typography, Button } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';

// ** Components 
import { KeteranganListTable } from '@modules/keterangan-config/components/table';


const KeteranganList = () => {
    const router = useRouter();
    const [keteranganData, setKeteranganData] = useState([]);
    const [isDataFetched, setIsDataFetched] = useState(false);

    const getKeteranganData = () => {
        apiGetAllKeteranganData().then(result => {
            setKeteranganData(result.data.data)
            setIsDataFetched(true)
        })
    }

    useEffect(() => {
        if (!keteranganData.length && !isDataFetched) {
            getKeteranganData()
        }
    }, [keteranganData, isDataFetched])

    const handleAdd = async () => {
        await router.push({
          pathname: `/system-config/keterangan/add`,
        });
    }

    const handleDelete = (id) => {
        apiDeleteKeterangan(id)
        setKeteranganData([])
        setIsDataFetched(false)
    }

    return(
        <>
            <Typography 
                variant="h3" 
                color="#E51C28"
                fontWeight={700}
                sx={{ 
                    my: 2
                }}
            >
                Keterangan Configuration
            </Typography>

            <Button 
                variant="contained" 
                endIcon={<AddIcon />} 
                color="success"
                onClick={handleAdd}
            >
                Tambah Keterangan
            </Button>
            
            {isDataFetched &&
                <KeteranganListTable data={keteranganData} onDelete={handleDelete} />
            }
        </>
    )
}

export default KeteranganList