// ** React Imports 
import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';

// ** MUI Components 
import { 
    Box,
    Button,
    Card,
    Grid,
    TextField,
    Typography,
} from '@mui/material';
import { apiGetKeteranganById, apiUpdateKeterangan } from 'lib/axios';

const KeteranganEdit = () => {
    const router = useRouter();
    const { id } = router.query;
    const [name, setName] = useState("")
    const [bgHex, setBgHex] = useState("");

    useEffect(() => {
        const getKeteranganData = async () => {
            apiGetKeteranganById(id).then(result => {
                setName(result.data.data.name)
                setBgHex(result.data.data.bgColor)
            })
        }

        if (id) {
            getKeteranganData()
        }
    }, [id])

    const handleName = (event) => {
        let value = event.target.value
        setName(value)
    }

    const handleBgHex = (event) => {
        let value = event.target.value
        setBgHex(value)
    }

    const getColorHex = (bg) => {
        const r = parseInt(bg.substr(1,2), 16)
        const g = parseInt(bg.substr(3,2), 16)
        const b = parseInt(bg.substr(5,2), 16)

        const brightness = Math.round(((parseInt(r) * 299) + (parseInt(g) * 587) + (parseInt(b) * 114)) / 1000);
        const result = brightness > 125 ? '#000' : '#fff'
        return result
    }

    const handleSubmit = (event) => {
        if (!name || !bgHex) {
            alert('Terdapat field yang belum diisi!')
            return
        }

        event.preventDefault()
        const colorHex = getColorHex(bgHex)
        const data = {
            "name": name,
            "bgColor": bgHex,
            "colorHex": colorHex
        }
        
        apiUpdateKeterangan(data, id).then(
            router.back()
        )
    }

    const handleBack = async () => {
        await router.back();
    }

    return(
        <>
            <Typography 
                variant="h3" 
                color="#E51C28"
                fontWeight={700}
                sx={{ 
                    my: 2
                }}
            >
                Edit Keterangan
            </Typography>

            <Card sx={{ p: 5 }}>
                <Typography variant="h5">Masukkan Data Keterangan</Typography>
                <Grid container rowSpacing={2} columnSpacing={{ md: 6 }} sx={{ my: 1 }}>
                    <Grid item xs={12}>
                        <TextField 
                            fullWidth 
                            name="name" 
                            label="Nama Keterangan" 
                            variant="outlined" 
                            required 
                            onChange={handleName}
                            value={name}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Box sx={{ display: 'flex', alignItems: 'center', gap: 2 }}>
                            <TextField label="Warna Keterangan" variant='outlined' required disabled value={bgHex} />
                            <input 
                                type="color" 
                                value={bgHex} 
                                onChange={handleBgHex} 
                                style={{ border: 'none', padding: 0, background: 'none', height: '50px', width: '50px' }}
                            />
                        </Box>
                    </Grid>
                </Grid>
            </Card>

            <Box sx={{ display: 'flex', justifyContent: 'flex-end', my: 4, gap: 3 }}>
                <Button variant="outlined" color="error" onClick={handleBack}>BATALKAN</Button>
                <Button variant="contained" onClick={handleSubmit}>SIMPAN</Button>
            </Box>
        </>
    )
}

export default KeteranganEdit