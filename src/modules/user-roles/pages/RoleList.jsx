// ** React Imports 
import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { apiGetAllRolesData, apiDeleteRoles } from 'lib/axios';

// ** MUI Components 
import { Typography, Button } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';

// ** Components 
import { RoleListTable } from '@modules/user-roles/components/table';


const RoleList = () => {
    const router = useRouter();
    const [rolesData, setRolesData] = useState([]);
    const [isDataFetched, setIsDataFetched] = useState(false);
    
    const getRolesData = () => {
        apiGetAllRolesData().then(result => {
            setRolesData(result.data.data)
            setIsDataFetched(true)
        })
    }

    useEffect(() => {
        if (!rolesData.length && !isDataFetched) {
            getRolesData()
        }
    }, [rolesData, isDataFetched])

    const handleAdd = async () => {
        await router.push({
          pathname: `/system-config/user-roles/add`,
        });
    }

    const handleDelete = (id) => {
        apiDeleteRoles(id)
        setRolesData([])
        setIsDataFetched(false)
    }

    return(
        <>
            <Typography 
                variant="h3" 
                color="#E51C28"
                fontWeight={700}
                sx={{ 
                    my: 2
                }}
            >
                User Roles
            </Typography>

            <Button 
                variant="contained" 
                endIcon={<AddIcon />} 
                color="success"
                onClick={handleAdd}
            >
                Tambah Role
            </Button>
            
            {isDataFetched &&
                <RoleListTable data={rolesData} onDelete={handleDelete} />
            }
        </>
    )
}

export default RoleList