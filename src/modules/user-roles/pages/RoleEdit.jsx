// ** React Imports 
import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';

// ** MUI Components 
import { 
    Box,
    Button,
    Card,
    Grid,
    TextField,
    Typography,
} from '@mui/material';
import { apiGetRoleById, apiUpdateRoles } from 'lib/axios';

const RoleEdit = () => {
    const router = useRouter();
    const { id } = router.query;
    const [role, setRole] = useState("");

    useEffect(() => {
        const getRoleData = async () => {
            apiGetRoleById(id).then(result => {
                setRole(result.data.data.name)
            })
        }

        if (id) {
            getRoleData()
        }
    }, [id])

    const handleRole = (event) => {
        let value = event.target.value
        setRole(value)
    }

    const handleSubmit = (event) => {
        if (!role) {
            alert('Terdapat field yang belum diisi!')
            return
        }

        event.preventDefault()
        const data = {
            "name": role
        }

        apiUpdateRoles(data, id).then(
            router.back()
        )
    }

    const handleBack = async () => {
        await router.back();
    }

    return(
        <>
            <Typography 
                variant="h3" 
                color="#E51C28"
                fontWeight={700}
                sx={{ 
                    my: 2
                }}
            >
                Edit Risk
            </Typography>

            <Card sx={{ p: 5 }}>
                <Typography variant="h5">Masukkan Data Role</Typography>
                <Grid container rowSpacing={2} columnSpacing={{ md: 6 }} sx={{ my: 1 }}>
                    <Grid item xs={12}>
                        <TextField 
                            fullWidth 
                            name='name'
                            id="outlined-basic" 
                            label="Nama Role" 
                            variant="outlined" 
                            required
                            onChange={handleRole}
                            value={role}
                        />
                    </Grid>
                </Grid>
            </Card>

            <Box sx={{ display: 'flex', justifyContent: 'flex-end', my: 4, gap: 3 }}>
                <Button variant="outlined" color="error" onClick={handleBack}>BATALKAN</Button>
                <Button variant="contained" onClick={handleSubmit}>SIMPAN</Button>
            </Box>
        </>
    )
}

export default RoleEdit