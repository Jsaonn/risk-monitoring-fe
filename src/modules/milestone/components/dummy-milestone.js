export const DUMMY_MILESTONE = [
    {
        "id": "1",
        "program": "Battery Replacement West",
        "value": "6.7",
        "progress": ["100", "100", "100", "0", "0", "0", "0", "0", "0", "0"],
        "totalWeek": "6",
        "milestone": [
            {
                "name": " Sourcing",
                "week": "6"
            },
        ],
    },
    {
        "id": "2",
        "program": "Battery Replacement East",
        "value": "3.2",
        "progress": ["100", "100", "100", "100", "100", "100", "100", "100", "100", "0"],
        "totalWeek": "6",
        "milestone": [
            {
                "name": " PO",
                "week": "6"
            },
        ],
    },
    {
        "id": "1",
        "program": "Battery Replacement West",
        "value": "34.8",
        "progress": ["100", "0", "0", "0", "0", "0", "0", "0", "0", "0"],
        "totalWeek": "6",
        "milestone": [
            {
                "name": " Capex Paper",
                "week": "5"
            },
            {
                "name": "KPAA",
                "week": "1"
            }
        ],
    },
]