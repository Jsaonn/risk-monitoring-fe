export const MONTH_LIST = [
    {
        "name": "SEP-2022",
        "weekCount": "4",
    },
    {
        "name": "OCT-2022",
        "weekCount": "4",
    },
    {
        "name": "NOV-2022",
        "weekCount": "5",
    },
    {
        "name": "DEC-2022",
        "weekCount": "4",
    },
]