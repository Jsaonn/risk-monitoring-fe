// ** React Imports 
import React, { useState, useEffect, Fragment } from 'react';
import { useRouter } from 'next/router';

// ** MUI Components 
import Box from "@mui/material/Box"
import Table from "@mui/material/Table"
import TableRow from "@mui/material/TableRow"
import TableHead from "@mui/material/TableHead"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import Typography from "@mui/material/Typography"
import IconButton from "@mui/material/IconButton"
import { TablePagination, TableContainer } from "@mui/material"
import Chip from '@mui/material/Chip';

import { MONTH_LIST } from '@modules/milestone/components/table';
import { numberToCurrency} from 'lib';
import { apiGetAllMilestoneData } from 'lib/axios';

const Row = ({ row, totalWeek, skipWeek, index, abbr }) => {

    return(
        <Fragment>
            <TableRow sx={{ background: '#fff' }}>
                <TableCell component='th' scope='row' sx={{ border: '1px solid #828282', position: 'sticky', left: 0, zIndex: 800, background: '#fff', borderRight: 'none' }}>
                    <Typography variant='body2'>
                        {row.name}
                    </Typography>
                </TableCell>
                <TableCell component='th' scope='row' align='center' sx={{ border: '1px solid #828282', position: 'sticky', left: "202px", zIndex: 800, background: '#fff' }}>
                    <Typography variant='body2'>
                        IDR {numberToCurrency(row.value)}
                    </Typography>
                </TableCell>
                <TableCell component='th' scope='row' align='center' colSpan={skipWeek[index]}></TableCell>
                {
                    row.programMilestoneData.map((item, index) => (
                        <TableCell key={index} colSpan={item.week} component='th' scope='row' align='center'>
                            <Chip label={abbr[item.name]} sx={{ background: '#2FC948', color: '#fff', width: '100%', display: item.week == 0 ? 'none' : 'flex', justifyContent: 'center' }} />
                        </TableCell>
                    ))
                }
                <TableCell component='th' scope='row' align='center' colSpan={totalWeek - row.totalWeek}></TableCell>
            </TableRow>
        </Fragment>
    )
}

export const MilestoneTable = ({ data, tableHeader, skipWeek }) => {
    const [width, setWidth] = useState(window.innerWidth)
    const [totalWeek, setTotalWeek] = useState(0)
    const [milestoneAbbr, setMilestoneAbbr] = useState({})
    const [isDataFetched, setIsDataFetched] = useState(false)
    const [page, setPage] = useState(0)
    const [rowsPerPage, setRowsPerPage] = useState(10)

    const router = useRouter()
    const { id } = router.query

    const updateDimensions = () => {
        setWidth(window.innerWidth)
    }

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    useEffect(() => {
        window.addEventListener("resize", updateDimensions)
        return () => window.removeEventListener("resize", updateDimensions)
    }, [])

    useEffect(() => {
        const getMilestoneData = async () => {
            const resultData = {}
            apiGetAllMilestoneData().then(result => {
                const fetchData = result.data.data.filter(item => item.ttc == id)

                if (fetchData) {
                    fetchData.map(item => {
                        resultData[item.name] = item.abbr
                    })
                    setMilestoneAbbr(resultData)
                    setIsDataFetched(true)
                }
            })
        }

        if (!isDataFetched) {
            getMilestoneData()
        }

        let count = 0
        tableHeader.map((item) => {
            count += item.weekCount
        })
        setTotalWeek(count)

    }, [isDataFetched, milestoneAbbr])

    return (
        <>
        {
            totalWeek &&
            <TableContainer sx={{ mt: 2, position: 'relative', overflow: 'auto', whiteSpace: 'nowrap', width: `calc(${width}px - 272px)`, borderRight: '1px solid #828282' }}>
                <Table aria-label="collapsible table" sx={{ borderCollapse: 'separate' }}>
                    <TableHead sx={{ backgroundColor: '#F9FAFC' }}>
                        <TableRow>
                            <TableCell align="center" rowSpan={2} 
                                sx={{ 
                                    width: "200px", 
                                    minWidth: "200px", 
                                    maxWidth: "200px", 
                                    fontWeight: 'bold', 
                                    textAlign: "left", 
                                    left: "0px",
                                    position: '-webkit-sticky',
                                    position: 'sticky',
                                    backgroundColor: '#F9FAFC',
                                    border: '1px solid #828282', 
                                    borderRight: 'none'
                                }}
                            >
                                PROGRAM
                            </TableCell>
                            <TableCell align="center" rowSpan={2} 
                                sx={{ 
                                    width: "100px", 
                                    minWidth: "100px", 
                                    maxWidth: "100px", 
                                    fontWeight: 'bold', 
                                    textAlign: "center", 
                                    left: "202px",
                                    position: '-webkit-sticky',
                                    position: 'sticky',
                                    backgroundColor: '#F9FAFC',
                                    border: '1px solid #828282', 
                                }}
                            >
                                VALUE
                            </TableCell>
                            {
                                tableHeader.map((item, index) => (
                                    <TableCell align="center" key={index} colSpan={item.weekCount} sx={{ top: 0, fontWeight: 'bold', border: '1px solid #828282' }}>
                                        <Typography variant="caption" fontWeight={700}>
                                            {item.name}
                                        </Typography>
                                    </TableCell>
                                ))
                            }
                        </TableRow>
                        <TableRow>
                            {
                                tableHeader.map((item) => (
                                    Array.from({length: (item.weekCount)}, (_, i) => i+1).map((week, index) => (
                                        <TableCell align="center" key={index} sx={{ top: 0, fontWeight: 'bold', border: '1px solid #828282', minWidth: '100px' }}>
                                            <Typography variant="caption" fontWeight={700}>
                                                W{week}
                                            </Typography>
                                        </TableCell>
                                    ))
                                ))
                            }
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {Object.keys(milestoneAbbr).length != 0 && data
                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        .map((row, i) => (
                            <Row
                                key={`row-${i}`}
                                row={row}
                                totalWeek={totalWeek}
                                index={i}
                                skipWeek={skipWeek}
                                abbr={milestoneAbbr}
                            />
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        }
        <TablePagination
            component="div"
            count={data.length}
            page={page}
            onPageChange={handleChangePage}
            rowsPerPage={rowsPerPage}
            onRowsPerPageChange={handleChangeRowsPerPage}
            sx={{ background: '#fff', border: '1px solid #828282' }}
        />
        </>
    )
}