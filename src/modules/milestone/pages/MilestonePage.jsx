// ** React Imports 
import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { apiGetAllProgramData } from 'lib/axios';

// ** MUI Components 
import { Box, Typography } from '@mui/material';

import { MilestoneTable } from '@modules/milestone/components/table';
import { DUMMY_MILESTONE } from '@modules/milestone/components';
import { getWeekMilestones } from 'lib';

const MilestonePage = () => {
    const [tableHeader, setTableHeader] = useState([])
    const [skipWeek, setSkipWeek] = useState([])
    const [programData, setProgramData] = useState([])
    const [isDataFetched, setIsDataFetched] = useState(false)

    const router = useRouter()
    const { id, ttc } = router.query

    const getProgramData = () => {
        if (id) {
            apiGetAllProgramData().then(result => {
                const fetchData = result.data.data.filter(item => item.ttc == id)
                
                if (fetchData.length) {
                    const { header, skipWeek } = getWeekMilestones(fetchData)
                    setProgramData(fetchData)
                    setIsDataFetched(true)
                    if (header && skipWeek) {
                        setTableHeader(header)
                        setSkipWeek(skipWeek)
                    }
                }
            })
        }
    }

    useEffect(() => {
        if (!programData.length && id) {
            getProgramData()
        }
    }, [programData, isDataFetched, id])

    return(
        <>
            <Typography 
                variant="h3" 
                color="#E51C28"
                fontWeight={700}
                sx={{ 
                    mt: 2
                }}
            >
                TTC {ttc} - Milestone
            </Typography>

            {tableHeader.length && skipWeek.length && programData.length &&
                <MilestoneTable data={programData} tableHeader={tableHeader} skipWeek={skipWeek} />
            }
        </>
    )
}

export default MilestonePage