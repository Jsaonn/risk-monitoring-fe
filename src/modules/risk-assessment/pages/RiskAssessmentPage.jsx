// ** React Imports 
import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';

// ** MUI Components 
import { Box, Typography, Grid, Button } from '@mui/material';

// ** Components 
import { ModalRisk, RISK_DATA } from '@modules/risk-assessment/components';
import { RISK_MANAGEMENT_DATA } from '@modules/risk-management/components';
import { RiskTable } from '@modules/risk-assessment/components/table';
import { apiGetAllKeteranganData, apiGetAllRiskData } from 'lib/axios';

const RiskAssessmentPage = () => {
    const [keteranganData, setKeteranganData] = useState([])
    const [riskData, setRiskData] = useState([])
    const [keteranganColor, setKeteranganColor] = useState([])
    const [isDataFetched, setIsDataFetched] = useState(false)
    const [modalOpen, setModalOpen] = useState(false)
    const [modalData, setModalData] = useState([])

    const router = useRouter()
    const { id, ttc } = router.query

    const getKeteranganData = () => {
        const resultData = {}
        apiGetAllKeteranganData().then(result => {
            result.data.data.map((item => {
                resultData[item.name] = {
                    "bgColor": item.bgColor,
                    "colorHex": item.colorHex
                }
            }))
            setKeteranganColor(resultData)
            setKeteranganData(result.data.data)
        })
    }

    const getRiskData = () => {
        if (id) {
            apiGetAllRiskData().then(result => {
                const fetchData = result.data.data.filter(item => item.ttc == id)

                if (fetchData) {
                    setRiskData(fetchData)
                }
            })
        }
    }

    useEffect(() => {
        if (!keteranganData.length || !riskData.length) {
            getKeteranganData()
            getRiskData()
            setIsDataFetched(true)

        }
    }, [riskData, keteranganData, isDataFetched])

    const handleModalOpen = async (data) => {
        await setModalData(data)
        setModalOpen(true)
    }

    const handleModalClose = () => {
        setModalOpen(false)
    }

    return (
      <>
        <Typography 
            variant="h3" 
            color="#E51C28"
            fontWeight={700}
            sx={{ 
                mt: 2
            }}
        >
            TTC {ttc} - Risk Register
        </Typography>

        {riskData && (
            <>
                <Box sx={{ display: 'flex', justifyContent: 'center', my: 5 }}>
                    <Grid container sx={{ background: '#fff', borderRadius: '10px', minHeight: '500px' }}>
                        {riskData && 
                            <>
                                <Grid item xs={6} sx={{ borderRight: '1px solid #000', borderBottom: '1px solid #000' }}>
                                    <Typography align="center" color="black" sx={{ mt: 2 }}>High Impact - Low Effort</Typography>
                                    <Box sx={{ display: 'flex', p: 2, m: 2, gap: 2, flexWrap: 'wrap', borderRadius: '10px', minHeight: '250px' }}>
                                        {
                                            riskData
                                            .filter((item) => item.impact === "HILE")
                                            .map((data, idx) => (
                                                <Button 
                                                    key={idx} 
                                                    variant="contained" 
                                                    onClick={()=>handleModalOpen(data)} 
                                                    sx={{ 
                                                        height: 64, 
                                                        width: 50, 
                                                        p: 0, 
                                                        borderRadius: '50%', 
                                                        background: keteranganColor[data.keterangan].bgColor,
                                                        color: keteranganColor[data.keterangan].colorHex,
                                                        '&:hover': {
                                                            background: keteranganColor[data.keterangan].bgColor
                                                        } 
                                                    }}>
                                                        {data.id}
                                                    </Button>
                                            ))
                                        }
                                    </Box>
                                </Grid>
                                <Grid item xs={6} sx={{ borderBottom: '1px solid #000', borderLeft: '1px solid #000' }}>
                                    <Typography align="center" color="black" sx={{ mt: 2 }}>High Impact - High Effort</Typography>
                                    <Box sx={{ display: 'flex', p: 2, m: 2, gap: 2, flexWrap: 'wrap', borderRadius: '10px', minHeight: '250px' }}>
                                        {
                                            riskData
                                            .filter((item) => item.impact === "HIHE")
                                            .map((data, idx) => (
                                                <Button 
                                                    key={idx} 
                                                    variant="contained" 
                                                    onClick={()=>handleModalOpen(data)} 
                                                    sx={{ 
                                                        height: 64, 
                                                        width: 50, 
                                                        p: 0, 
                                                        borderRadius: '50%', 
                                                        background: keteranganColor[data.keterangan].bgColor,
                                                        color: keteranganColor[data.keterangan].colorHex,
                                                        '&:hover': {
                                                            background: keteranganColor[data.keterangan].bgColor
                                                        } 
                                                    }}>
                                                        {data.id}
                                                    </Button>
                                            ))
                                        }
                                    </Box>
                                </Grid>
                                <Grid item xs={6} sx={{ borderRight: '1px solid #000', borderTop: '1px solid #000' }}>
                                    <Typography align="center" color="black" sx={{ mt: 2 }}>Low Impact - Low Effort</Typography>
                                    <Box sx={{ display: 'flex', p: 2, m: 2, gap: 2, flexWrap: 'wrap', borderRadius: '10px', minHeight: '250px' }}>
                                        {
                                            riskData
                                            .filter((item) => item.impact === "LILE")
                                            .map((data, idx) => (
                                                <Button 
                                                    key={idx} 
                                                    variant="contained" 
                                                    onClick={()=>handleModalOpen(data)} 
                                                    sx={{ 
                                                        height: 64, 
                                                        width: 50, 
                                                        p: 0, 
                                                        borderRadius: '50%', 
                                                        background: keteranganColor[data.keterangan].bgColor,
                                                        color: keteranganColor[data.keterangan].colorHex,
                                                        '&:hover': {
                                                            background: keteranganColor[data.keterangan].bgColor
                                                        } 
                                                    }}>
                                                        {data.id}
                                                    </Button>
                                            ))
                                        }
                                    </Box>
                                </Grid>
                                <Grid item xs={6} sx={{ borderTop: '1px solid #000', borderLeft: '1px solid #000' }}>
                                    <Typography align="center" color="black" sx={{ mt: 2 }}>Low Impact - High Effort</Typography>
                                    <Box sx={{ display: 'flex', p: 2, m: 2, gap: 2, flexWrap: 'wrap', borderRadius: '10px', minHeight: '250px' }}>
                                        {
                                            riskData
                                            .filter((item) => item.impact === "LIHE")
                                            .map((data, idx) => (
                                                <Button 
                                                    key={idx} 
                                                    variant="contained" 
                                                    onClick={()=>handleModalOpen(data)} 
                                                    sx={{ 
                                                        height: 64, 
                                                        width: 50, 
                                                        p: 0, 
                                                        borderRadius: '50%', 
                                                        background: keteranganColor[data.keterangan].bgColor,
                                                        color: keteranganColor[data.keterangan].colorHex,
                                                        '&:hover': {
                                                            background: keteranganColor[data.keterangan].bgColor
                                                        } 
                                                    }}>
                                                        {data.id}
                                                    </Button>
                                            ))
                                        }
                                    </Box>
                                </Grid>
                            </>
                        }
                    </Grid>
                    <ModalRisk open={modalOpen} onClose={handleModalClose} data={modalData} />
                </Box>

                <RiskTable data={riskData} keteranganColor={keteranganColor} />
            </>
        )}

        {/** KETERANGAN */}
        <Box sx={{ mt: 5 }}>
            <Typography variant="h4" fontWeight={700} color="black">Keterangan</Typography>

            <Grid container spacing={3} sx={{ mt: 2 }}>
                {keteranganData && keteranganData.map((keterangan) => (
                    <Grid container item xs={4} alignItems="center">
                        <Grid item xs={2}>
                            <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', gap: '2px' }}>
                                <Box sx={{ height: 30, width: 30, borderRadius: '50%', background: keterangan.bgColor }} />
                            </Box>
                        </Grid>
                        <Grid item xs={10}>
                            <Typography variant="body2" color="black">{keterangan.name}</Typography>
                        </Grid>
                    </Grid>
                ))}
            </Grid>
        </Box>
      </>
    );
  }

export default RiskAssessmentPage;