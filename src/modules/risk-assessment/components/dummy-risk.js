export const DUMMY_TTC = [
    {
        "id": "1",
        "title": "BSD"
    },
    {
        "id": "2",
        "title": "TBS"
    },
    {
        "id": "3",
        "title": "BUARAN"
    },
    {
        "id": "4",
        "title": "ARIFIN AHMAD"
    },
    {
        "id": "5",
        "title": "SOLO BARU"
    },
    {
        "id": "6",
        "title": "SUDIANG"
    },
    {
        "id": "7",
        "title": "GAYUNGAN"
    },
]

export const RISK_DATA = [
    {
        "id": "1",
        "name": "Risiko struktur gedung terhadap gempa bumi",
        "type": "struktur",
        "consequence": 3,
        "likelihood": 4,
        "level": "VH",
        "impact": "HILE",
    },
    {
        "id": "2",
        "name": "Risiko struktur gedung terhadap kebakaran",
        "type": "struktur",
        "consequence": 3,
        "likelihood": 4,
        "level": "VH",
        "impact": "HIHE",
    },
    {
        "id": "3",
        "name": "Risiko struktur gedung terhadap pergeseran tanah",
        "type": "struktur",
        "consequence": 3,
        "likelihood": 4,
        "level": "VH",
        "impact": "LILE",
    },
    {
        "id": "4",
        "name": "Dokumen call tree belum lengkap",
        "type": "struktur",
        "consequence": 3,
        "likelihood": 4,
        "level": "VH",
        "impact": "LIHE",
    },
    {
        "id": "5",
        "name": "Terhambatnya evakuasi pada keadaan darurat",
        "type": "struktur",
        "consequence": 3,
        "likelihood": 4,
        "level": "VH",
        "impact": "HILE",
    },
    {
        "id": "6",
        "name": "Penampungan air keruh menghambat penanganan insiden kebakaran",
        "type": "struktur",
        "consequence": 3,
        "likelihood": 4,
        "level": "VH",
        "impact": "HIHE",
    },
]