// ** React Imports 
import React, { useState, Fragment, useEffect } from 'react';

// ** MUI Components 
import Table from "@mui/material/Table"
import TableRow from "@mui/material/TableRow"
import TableHead from "@mui/material/TableHead"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import Typography from "@mui/material/Typography"
import IconButton from "@mui/material/IconButton"
import { TablePagination, TableContainer } from "@mui/material"
import { styled } from "@mui/material/styles"
import Chip from '@mui/material/Chip';
import { apiGetAllKeteranganData } from 'lib/axios';

const Row = ({ row, index, keteranganColor }) => {

    return(
        <Fragment>
            <TableRow sx={{ background: '#fff' }}>
                <TableCell component='th' scope='row' align='center' sx={{ border: '1px solid #828282' }}>
                    <Typography variant='body2'>
                        {index}
                    </Typography>
                </TableCell>
                <TableCell component='th' scope='row' sx={{ border: '1px solid #828282' }}>
                    <Typography variant='body2'>
                        {row.name}
                    </Typography>
                </TableCell>
                <TableCell component='th' scope='row' align='center' sx={{ border: '1px solid #828282' }}>
                    <Typography variant='body2'>
                        {row.id}
                    </Typography>
                </TableCell>
                <TableCell component='th' scope='row' align='center' sx={{ border: '1px solid #828282' }}>
                <Chip label={row.keterangan.replace(/ .*/,'')} sx={{ background: keteranganColor[row.keterangan].bgColor, color: keteranganColor[row.keterangan].colorHex }} />
                </TableCell>
                <TableCell component='th' scope='row' align='center' sx={{ border: '1px solid #828282' }}>
                    <Typography variant='body2'>
                        {row.consequence}
                    </Typography>
                </TableCell>
                <TableCell component='th' scope='row' align='center' sx={{ border: '1px solid #828282' }}>
                    <Typography variant='body2'>
                        {row.likelihood}
                    </Typography>
                </TableCell>
                <TableCell component='th' scope='row' align='center' sx={{ border: '1px solid #828282' }}>
                    <Typography variant='body2'>
                        {row.level}
                    </Typography>
                </TableCell>
            </TableRow>
        </Fragment>
    )
}

export const RiskTable = ({ data, keteranganColor }) => {
    const [page, setPage] = useState(0)
    const [rowsPerPage, setRowsPerPage] = useState(10)

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };
    
    return (
        <>
            <TableContainer sx={{ mt: 4 }}>
                <Table aria-label="collapsible table">
                    <TableHead sx={{ backgroundColor: '#F9FAFC' }}>
                        <TableRow>
                            <TableCell align="center" sx={{ minWidth: "50px", fontWeight: 'bold', border: '1px solid #828282' }}>
                                No
                            </TableCell>
                            <TableCell sx={{ minWidth: "500px", fontWeight: 'bold', border: '1px solid #828282' }}>
                                Nama Risiko
                            </TableCell>
                            <TableCell align="center" sx={{ minWidth: "50px", fontWeight: 'bold', border: '1px solid #828282' }}>
                                ID Risiko
                            </TableCell>
                            <TableCell align="center" sx={{ minWidth: "150px", fontWeight: 'bold', border: '1px solid #828282' }}>
                                Tipe
                            </TableCell>
                            <TableCell align="center" sx={{ minWidth: "150px", fontWeight: 'bold', border: '1px solid #828282' }}>
                                Consequences
                            </TableCell>
                            <TableCell align="center" sx={{ minWidth: "150px", fontWeight: 'bold', border: '1px solid #828282' }}>
                                Likelihood
                            </TableCell>
                            <TableCell align="center" sx={{ minWidth: "150px", fontWeight: 'bold', border: '1px solid #828282' }}>
                                Level of Risk
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data
                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        .map((row, i) => (
                            <Row
                                key={`row-${i}`}
                                row={row}
                                index={i+1}
                                keteranganColor={keteranganColor}
                            />
                        ))}
                    </TableBody>
                </Table>
                <TablePagination
                    component="div"
                    count={data.length}
                    page={page}
                    onPageChange={handleChangePage}
                    rowsPerPage={rowsPerPage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    sx={{ background: '#fff', border: '1px solid #828282' }}
                />
            </TableContainer>
        </>
    )
}