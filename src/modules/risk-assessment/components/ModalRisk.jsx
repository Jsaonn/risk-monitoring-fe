// ** React Imports 
import React from 'react';

// ** MUI Components 
import {
    Box,
    Button,
    Dialog,
    Grid,
    Typography,
} from "@mui/material"

export const ModalRisk = ({ open, onClose, data }) => {

    return(
        <Dialog open={open} onClose={onClose}>
            <Box
                sx={{ 
                    minWidth: { md: 600, xs: '100%' },
                    px: 10,
                    py: '50px',
                 }}
            >
                <Grid container>
                    <Grid item xs={4}>
                        <Typography variant="body2" color="black" fontWeight={700}>Risk name</Typography>
                    </Grid>
                    <Grid item xs={1}>
                        <Typography variant="body2" color="black" fontWeight={700} align="center">:</Typography>
                    </Grid>
                    <Grid item xs={7}>
                        <Typography variant="body2" color="black">{data.name}</Typography>
                    </Grid>

                    <Grid item xs={4}>
                        <Typography variant="body2" color="black" fontWeight={700}>Risk ID</Typography>
                    </Grid>
                    <Grid item xs={1}>
                        <Typography variant="body2" color="black" fontWeight={700} align="center">:</Typography>
                    </Grid>
                    <Grid item xs={7}>
                        <Typography variant="body2" color="black">{data.id}</Typography>
                    </Grid>

                    <Grid item xs={4}>
                        <Typography variant="body2" color="black" fontWeight={700}>Type</Typography>
                    </Grid>
                    <Grid item xs={1}>
                        <Typography variant="body2" color="black" fontWeight={700} align="center">:</Typography>
                    </Grid>
                    <Grid item xs={7}>
                        <Typography variant="body2" color="black">{data.keterangan}</Typography>
                    </Grid>

                    <Grid item xs={4}>
                        <Typography variant="body2" color="black" fontWeight={700}>Consequences</Typography>
                    </Grid>
                    <Grid item xs={1}>
                        <Typography variant="body2" color="black" fontWeight={700} align="center">:</Typography>
                    </Grid>
                    <Grid item xs={7}>
                        <Typography variant="body2" color="black">{data.consequence}</Typography>
                    </Grid>

                    <Grid item xs={4}>
                        <Typography variant="body2" color="black" fontWeight={700}>Likelihood</Typography>
                    </Grid>
                    <Grid item xs={1}>
                        <Typography variant="body2" color="black" fontWeight={700} align="center">:</Typography>
                    </Grid>
                    <Grid item xs={7}>
                        <Typography variant="body2" color="black">{data.likelihood}</Typography>
                    </Grid>

                    <Grid item xs={4}>
                        <Typography variant="body2" color="black" fontWeight={700}>Level of risk</Typography>
                    </Grid>
                    <Grid item xs={1}>
                        <Typography variant="body2" color="black" fontWeight={700} align="center">:</Typography>
                    </Grid>
                    <Grid item xs={7}>
                        <Typography variant="body2" color="black">{data.level}</Typography>
                    </Grid>
                </Grid>

                <Box sx={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center', gap: 2, mt: 2 }}>
                    <Button variant="contained" onClick={onClose}>TUTUP</Button>
                </Box>
            </Box>
        </Dialog>
    )
}