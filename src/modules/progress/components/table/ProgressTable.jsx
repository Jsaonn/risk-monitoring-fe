// ** React Imports 
import React, { useState, useEffect, Fragment } from 'react';
import { useRouter } from 'next/router';

// ** MUI Components 
import Box from "@mui/material/Box"
import Table from "@mui/material/Table"
import TableRow from "@mui/material/TableRow"
import TableHead from "@mui/material/TableHead"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import Typography from "@mui/material/Typography"
import IconButton from "@mui/material/IconButton"
import { TablePagination, TableContainer } from "@mui/material"

import { numberToCurrency, getAverageProgress } from 'lib';

const Row = ({ row }) => {

    return(
        <Fragment>
            <TableRow sx={{ background: '#fff' }}>
                <TableCell component='th' scope='row' sx={{ border: '1px solid #828282', position: 'sticky', left: 0, zIndex: 800, background: '#fff', borderRight: 'none' }}>
                    <Typography variant='body2'>
                        {row.name}
                    </Typography>
                </TableCell>
                <TableCell component='th' scope='row' align='center' sx={{ border: '1px solid #828282', position: 'sticky', left: "202px", zIndex: 800, background: '#fff' }}>
                    <Typography variant='body2'>
                        IDR {numberToCurrency(row.value)}
                    </Typography>
                </TableCell>
                {
                    row.programMilestoneData.map((milestone) => (
                        <TableCell component='th' scope='row' align='center' sx={{ border: '1px solid #828282' }}>
                            <Typography variant='body2'>
                                {milestone.progress}%
                            </Typography>
                        </TableCell>
                    ))
                }
            </TableRow>
        </Fragment>
    )
}

export const ProgressTable = ({ data, milestone }) => {
    const [width, setWidth] = useState(window.innerWidth)
    const [page, setPage] = useState(0)
    const [rowsPerPage, setRowsPerPage] = useState(10)
    const [averageProgress, setAverageProgress] = useState([])

    const updateDimensions = () => {
        setWidth(window.innerWidth)
    }

    useEffect(() => {
        window.addEventListener("resize", updateDimensions)
        console.log(width);
        return () => window.removeEventListener("resize", updateDimensions)
    }, [])

    useEffect(() => {
        if (!averageProgress.length) {
            const average = getAverageProgress(data, milestone.length)
            setAverageProgress(average)
        }
        
    }, [averageProgress])

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    return (
        <>
            <TableContainer sx={{ mt: 2, position: 'relative', overflow: 'auto', whiteSpace: 'nowrap', width: `calc(${width}px - 272px)`, borderRight: '1px solid #828282' }}>
                <Table aria-label="collapsible table" sx={{ borderCollapse: 'separate' }}>
                    <TableHead sx={{ backgroundColor: '#F9FAFC' }}>
                        <TableRow>
                            <TableCell align="center" rowSpan={3} 
                                sx={{ 
                                    width: "200px", 
                                    minWidth: "200px", 
                                    maxWidth: "200px", 
                                    fontWeight: 'bold', 
                                    textAlign: "left", 
                                    left: "0px",
                                    position: '-webkit-sticky',
                                    position: 'sticky',
                                    backgroundColor: '#F9FAFC',
                                    border: '1px solid #828282', 
                                    borderRight: 'none'
                                }}
                            >
                                PROGRAM
                            </TableCell>
                            <TableCell align="center" rowSpan={3} 
                                sx={{ 
                                    width: "100px", 
                                    minWidth: "100px", 
                                    maxWidth: "100px", 
                                    fontWeight: 'bold', 
                                    textAlign: "center", 
                                    left: "202px",
                                    position: '-webkit-sticky',
                                    position: 'sticky',
                                    backgroundColor: '#F9FAFC',
                                    border: '1px solid #828282', 
                                }}
                            >
                                VALUE
                            </TableCell>
                            {
                                milestone.map((roles, index) => (
                                    <TableCell align="center" key={index} sx={{ top: 0, fontWeight: 'bold', border: '1px solid #828282' }}>
                                        {roles.milestoneRolesData.map((role, idx) => (
                                            <Typography variant="caption" fontWeight={700} key={idx}>
                                                {role === roles.milestoneRolesData[roles.milestoneRolesData.length-1] ? role.name : `${role.name}, `}
                                            </Typography>
                                        ))}
                                    </TableCell>
                                ))
                            }
                        </TableRow>

                        <TableRow>
                            {
                                milestone.map((item, index) => (
                                    <TableCell align="center" key={index} sx={{ top: 0, fontWeight: 'bold', border: '1px solid #828282' }}>
                                        <Typography variant="body1" fontWeight={700}>
                                            {item.abbr}
                                        </Typography>
                                    </TableCell>
                                ))
                            }
                        </TableRow>

                        <TableRow>
                            {
                                averageProgress.map((progress, index) => (
                                    <TableCell align="center" key={index} sx={{ top: 0, fontWeight: 'bold', border: '1px solid #828282' }}>
                                        <Typography variant="body2" fontWeight={700}>
                                            {progress}%
                                        </Typography>
                                    </TableCell>
                                ))
                            }
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data
                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        .map((row, i) => (
                            <Row
                                key={`row-${i}`}
                                row={row}
                            />
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                component="div"
                count={data.length}
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={rowsPerPage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                sx={{ background: '#fff', border: '1px solid #828282' }}
            />
        </>
    )
}