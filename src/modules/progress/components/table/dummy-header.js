export const USER_LIST = [
    "Business User",
    "FBP",
    "Business User & Procurement",
    "Procurement",
    "Procurement",
    "Business User & FBP",
    "Business User",
    "Business User & FBP",
    "Business User",
    "Procurement"
]

export const MILESTONE_LIST = [
    "CP",
    "KPAA",
    "DP",
    "SOU.",
    "OA",
    "EP. ID KPAA ID",
    "WBS",
    "Pre PR",
    "PR",
    "PO",
]

export const MILESTONE_PROGRESS = [
    "100%",
    "22%",
    "22%",
    "7%",
    "7%",
    "7%",
    "7%",
    "7%",
    "7%",
    "0%",
]