export const DUMMY_PROGRESS = [
    {
        "id": "1",
        "program": "Battery Replacement West",
        "value": "6.7",
        "progress": ["100", "100", "100", "0", "0", "0", "0", "0", "0", "0"],
    },
    {
        "id": "2",
        "program": "Battery Replacement East",
        "value": "3.2",
        "progress": ["100", "100", "100", "100", "100", "100", "100", "100", "100", "0"],
    },
    {
        "id": "1",
        "program": "Battery Replacement West",
        "value": "34.8",
        "progress": ["100", "0", "0", "0", "0", "0", "0", "0", "0", "0"],
    },
]