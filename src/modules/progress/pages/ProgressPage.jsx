// ** React Imports 
import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';

// ** MUI Components 
import { Box, Typography } from '@mui/material';

import { ProgressTable } from '@modules/progress/components/table';
import { DUMMY_PROGRESS } from "@modules/progress/components";
import { apiGetAllMilestoneData, apiGetAllProgramData } from 'lib/axios';

const ProgressPage = () => {
    const [programData, setProgramData] = useState([])
    const [milestoneData, setMilestoneData] = useState([])
    const [isDataFetched, setIsDataFetched] = useState(false)

    const router = useRouter()
    const { id, ttc } = router.query

    const getProgramData = () => {
        if (id) {
            apiGetAllProgramData().then(result => {
                const fetchData = result.data.data.filter(item => item.ttc == id)

                if (fetchData) {
                    setProgramData(fetchData)
                }
            })
        }
    }

    const getMilestoneData = () => {
        if (id) {
            apiGetAllMilestoneData().then(result => {
                const fetchData = result.data.data.filter(item => item.ttc == id)

                if (fetchData) {
                    setMilestoneData(fetchData)
                }
            })
        }
    }

    useEffect(() => {
        if ((!programData.length || !milestoneData.length) && id) {
            getProgramData()
            getMilestoneData()
            setIsDataFetched(true)
        }
    }, [programData, milestoneData, isDataFetched, id])

    return(
        <>
            <Typography 
                variant="h3" 
                color="#E51C28"
                fontWeight={700}
                sx={{ 
                    mt: 2
                }}
            >
                TTC {ttc} - Progress
            </Typography>

            {programData.length && milestoneData.length &&
                <ProgressTable data={programData} milestone={milestoneData} />
            }
        </>
    )
}

export default ProgressPage