// ** React Imports 
import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { apiGetAllTtcData, apiDeleteTtc } from 'lib/axios';

// ** MUI Components 
import { Typography, Button } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';

// ** Components 
import { TtcListTable } from '@modules/ttc-config/components/table'


const TtcList = () => {
    const router = useRouter();
    const [ttcData, setTtcData] = useState([]);
    const [isDataFetched, setIsDataFetched] = useState(false);

    const getTtcData = () => {
        apiGetAllTtcData().then(result => {
            setTtcData(result.data.data)
            setIsDataFetched(true)
        })
    }
    
    useEffect(() => {
        if (!ttcData.length && !isDataFetched) {
            getTtcData()
        }
    }, [ttcData, isDataFetched])

    const handleAdd = async () => {
        await router.push({
          pathname: `/system-config/ttc/add`,
        });
    }

    const handleDelete = (id) => {
        apiDeleteTtc(id)
        setTtcData([])
        setIsDataFetched(false)
    }

    return(
        <>
            <Typography 
                variant="h3" 
                color="#E51C28"
                fontWeight={700}
                sx={{ 
                    my: 2
                }}
            >
                TTC Configuration
            </Typography>

            <Button 
                variant="contained" 
                endIcon={<AddIcon />} 
                color="success"
                onClick={handleAdd}
            >
                Tambah TTC
            </Button>
            
            {isDataFetched &&
                <TtcListTable data={ttcData} onDelete={handleDelete} />
            }
        </>
    )
}

export default TtcList