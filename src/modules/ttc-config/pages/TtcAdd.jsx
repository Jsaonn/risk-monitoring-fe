// ** React Imports 
import React, { useState } from 'react';
import { useRouter } from 'next/router';

// ** MUI Components 
import { 
    Box,
    Button,
    Card,
    Grid,
    TextField,
    Typography,
} from '@mui/material';
import { apiCreateTtc } from 'lib/axios';

const TtcAdd = () => {
    const router = useRouter();
    const [ttc, setTtc] = useState("");

    const handleTtc = (event) => {
        let value = event.target.value
        setTtc(value)
    }

    const handleSubmit = (event) => {
        if (!ttc) {
            alert('Terdapat field yang belum diisi!')
            return
        }

        event.preventDefault()
        const data = {
            "name": ttc
        }

        apiCreateTtc(data).then(
            router.back()
        )
    }

    const handleBack = async () => {
        await router.back();
    }

    return(
        <>
            <Typography 
                variant="h3" 
                color="#E51C28"
                fontWeight={700}
                sx={{ 
                    my: 2
                }}
            >
                Add TTC
            </Typography>

            <Card sx={{ p: 5 }}>
                <Typography variant="h5">Masukkan Data TTC</Typography>
                <Grid container rowSpacing={2} columnSpacing={{ md: 6 }} sx={{ my: 1 }}>
                    <Grid item xs={12}>
                        <TextField 
                            fullWidth 
                            name="name" 
                            label="Nama TTC" 
                            variant="outlined" 
                            required 
                            onChange={handleTtc}
                            value={ttc}
                        />
                    </Grid>
                </Grid>
            </Card>

            <Box sx={{ display: 'flex', justifyContent: 'flex-end', my: 4, gap: 3 }}>
                <Button variant="outlined" color="error" onClick={handleBack}>BATALKAN</Button>
                <Button variant="contained" onClick={handleSubmit}>SIMPAN</Button>
            </Box>
        </>
    )
}

export default TtcAdd