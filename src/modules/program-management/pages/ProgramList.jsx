// ** React Imports 
import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { apiGetAllProgramData, apiDeleteProgram, apiGetAllMilestoneData } from 'lib/axios';

// ** MUI Components 
import { Box, Typography, Grid, Button } from '@mui/material';

// ** Components 
import { DUMMY_PROGRAM } from '@modules/program-management/components';
import { ProgramListTable } from '@modules/program-management/components/table';

import AddIcon from '@mui/icons-material/Add';

const ProgramList = () => {
    const [programData, setProgramData] = useState([])
    const [isDataFetched, setIsDataFetched] = useState(false);

    const router = useRouter();
    const { id, ttc } = router.query

    const getProgramData = () => {
        if (id) {
            apiGetAllProgramData().then(result => {
                const fetchData = result.data.data.filter(item => item.ttc == id)

                if (fetchData) {
                    setProgramData(fetchData)
                    setIsDataFetched(true)
                }
            })
        }
    }

    useEffect(() => {
        if (!programData.length && !isDataFetched) {
            getProgramData()
        }
    }, [programData, isDataFetched, id])

    const handleAdd = async () => {
        const getMilestoneData = async () => {
            if (id) {
                apiGetAllMilestoneData().then(result => {
                    const fetchData = result.data.data.filter(item => item.ttc == id)
                    
                    if (!fetchData.length) {
                        alert('Program tidak dapat ditambah karena milestone untuk TTC ini belum dibuat!')
                        return
                    } else {
                        router.push({
                            pathname: `/dashboard/[id]/program-management/add`,
                            query: { 
                              id: id,
                              ttc: ttc
                            },
                        });
                    }
                })
            }
        }
        getMilestoneData()
    }

    const handleDelete = (id) => {
        apiDeleteProgram(id)
        setProgramData([])
        setIsDataFetched(false)
    }

    return(
        <>
            <Typography 
                variant="h3" 
                color="#E51C28"
                fontWeight={700}
                sx={{ 
                    mt: 2
                }}
            >
                TTC {ttc} - Program Management
            </Typography>

            <Button 
                variant="contained" 
                endIcon={<AddIcon />} 
                color="success"
                onClick={handleAdd}
            >
                Tambah Program
            </Button>

            {programData && 
                <ProgramListTable data={programData} onDelete={handleDelete} />
            }
        </>
    )
}

export default ProgramList