// ** React Imports 
import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import Cookies from "js-cookie"

// ** MUI Components 
import { 
    Box,
    Button,
    Card,
    Grid,
    TextField,
    Typography,
} from '@mui/material';

import { DUMMY_PROGRAM } from '@modules/program-management/components';
import { apiGetProgramById, apiUpdateProgram, apiGetAllMilestoneData } from 'lib/axios';

const ProgramEdit = () => {
    const [name, setName] = useState("")
    const [value, setValue] = useState(0)
    const [milestone, setMilestone] = useState([])
    const [isDisabled, setIsDisabled] = useState({})

    const router = useRouter();
    const { id, programId, ttc } = router.query;

    useEffect(() => {
        const getProgramData = async () => {
            const resultDisabled = {}
            apiGetProgramById(programId).then(result => {
                result.data.data.programMilestoneData.map((item, index) => {
                    checkDisabled(item, index, resultDisabled)
                })
                setName(result.data.data.name)
                setValue(result.data.data.value)
                setMilestone(result.data.data.programMilestoneData)
            })
        }

        if (programId) {
            getProgramData()
        }
    }, [programId])

    const handleName = (event) => {
        let value = event.target.value
        setName(value)
    }

    const handleValue = (event) => {
        let value = event.target.value
        setValue(value)
    }

    const handleMilestone = index => event => {
        let copy = [...milestone]
        copy[index]["progress"] = event.target.value
        setMilestone(copy)
    }

    const handleSubmit = (event) => {
        if (!name || !value) {
            alert('Terdapat field yang belum diisi!')
            return
        }

        event.preventDefault()
        const data = {
            "name": name,
            "value": value,
            "programMilestoneData": milestone
        }

        apiUpdateProgram(data, programId).then(
            router.back()
        )
    }

    const checkDisabled = async (item, index, resultDisabled) => {
        if (item.progress === 100) {
            resultDisabled[index] = true
            setIsDisabled(resultDisabled)
        } else {
            const user = Cookies.get('role')
            const getMilestoneData = async () => {
                try {
                    let roles = []
                    if (id) {
                        const temp = await apiGetAllMilestoneData().then(result => {
                            const fetchData = result.data.data.filter(item => item.ttc == id)
            
                            if (fetchData[index]) {
                                fetchData[index].milestoneRolesData.map(milestone => {
                                    roles.push(milestone.name)
                                })
                            }
        
                            return roles
                        })
                        return temp
                    }
                } catch (err) {
                    throw err
                }
            }
    
            let result
            let temp
            temp = await getMilestoneData()
            if(temp.includes(user)) {
                result = false
            } else {
                result = true
            }
            resultDisabled[index] = result
            setIsDisabled(resultDisabled)
        }
    }

    const handleBack = async () => {
        await router.back();
    }

    return(
        <>
            <Typography 
                variant="h3" 
                color="#E51C28"
                fontWeight={700}
                sx={{ 
                    mt: 2
                }}
            >
                TTC {ttc} - Edit Program
            </Typography>

            <Card sx={{ p: 5 }}>
                <Typography variant="h5">Masukkan Data Program</Typography>
                <Grid container rowSpacing={2} columnSpacing={{ md: 6 }} sx={{ mt: 1, mb: 3 }}>
                    <Grid item xs={12}>
                        <TextField 
                            fullWidth 
                            id="outlined-basic" 
                            label="Nama TTC" 
                            variant="outlined" 
                            disabled 
                            value={ttc} 
                            InputLabelProps={{ shrink: true }} 
                        />
                    </Grid>

                    <Grid item xs={6}>
                        <TextField 
                            fullWidth 
                            id="outlined-basic" 
                            label="Nama Program" 
                            variant="outlined" 
                            required
                            onChange={handleName}
                            value={name}
                        />
                    </Grid>

                    <Grid item xs={6}>
                        <TextField 
                            fullWidth 
                            id="outlined-basic" 
                            label="Value" 
                            variant="outlined" 
                            required
                            onChange={handleValue}
                            value={value}
                        />
                    </Grid>
                </Grid>

                <Typography variant="h5">Edit Data Progres</Typography>
                {milestone.length !== 0  && (
                    <Grid container rowSpacing={2} columnSpacing={{ md: 6 }} sx={{ my: 1 }}>
                        {isDisabled && milestone.map((item, index) => {
                            ('index: ', index);
                            console.log(isDisabled[index])
                            console.log(isDisabled);
                            return (
                                <>
                                    <Grid item xs={6}>
                                        <Typography variant="body2">{item.name}</Typography>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <TextField 
                                            fullWidth
                                            id={item.id}
                                            label="Progres"
                                            variant="outlined"
                                            InputLabelProps={{ shrink: true }} 
                                            value={item.progress}
                                            disabled={isDisabled[index]}
                                            onChange={handleMilestone(index)}
                                        />
                                    </Grid>
                                </>
                            )
                        }
                        )}
                    </Grid>
                )}
            </Card>

            <Box sx={{ display: 'flex', justifyContent: 'flex-end', my: 4, gap: 3 }}>
                <Button variant="outlined" color="error" onClick={handleBack}>BATALKAN</Button>
                <Button variant="contained" onClick={handleSubmit}>SIMPAN</Button>
            </Box>
        </>
    )
}

export default ProgramEdit