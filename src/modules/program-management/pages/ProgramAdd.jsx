// ** React Imports 
import React, { useState } from 'react';
import { useRouter } from 'next/router';

// ** MUI Components 
import { 
    Box,
    Button,
    Card,
    Grid,
    TextField,
    Typography,
} from '@mui/material';
import { apiCreateProgram } from 'lib/axios';

const ProgramAdd = () => {
    const [name, setName] = useState("")
    const [value, setValue] = useState(0)

    const router = useRouter();
    const { id, ttc } = router.query;

    const handleName = (event) => {
        let value = event.target.value
        setName(value)
    }

    const handleValue = (event) => {
        let value = event.target.value
        setValue(value)
    }

    const handleSubmit = (event) => {
        if (!name || !value) {
            alert('Terdapat field yang belum diisi!')
            return
        }

        event.preventDefault()
        const data = {
            "ttc": id,
            "ttcName": ttc,
            "name": name,
            "value": value
        }

        apiCreateProgram(data).then(
            router.back()
        )
    }

    const handleBack = async () => {
        await router.back();
    }

    return(
        <>
            <Typography 
                variant="h3" 
                color="#E51C28"
                fontWeight={700}
                sx={{ 
                    mt: 2
                }}
            >
                TTC {ttc} - Add Program
            </Typography>

            <Card sx={{ p: 5 }}>
                <Typography variant="h5">Masukkan Data Program</Typography>
                <Grid container rowSpacing={2} columnSpacing={{ md: 6 }} sx={{ my: 1 }}>
                    <Grid item xs={12}>
                        <TextField 
                            fullWidth 
                            id="outlined-basic" 
                            label="Nama TTC" 
                            variant="outlined" 
                            disabled 
                            value={ttc} 
                            InputLabelProps={{ shrink: true }} 
                        />
                    </Grid>

                    <Grid item xs={6}>
                        <TextField 
                            fullWidth 
                            id="outlined-basic" 
                            label="Nama Program" 
                            variant="outlined" 
                            required
                            onChange={handleName}
                            value={name}
                        />
                    </Grid>

                    <Grid item xs={6}>
                        <TextField 
                            fullWidth 
                            id="outlined-basic" 
                            label="Value" 
                            variant="outlined" 
                            required
                            onChange={handleValue}
                            value={value}
                        />
                    </Grid>
                </Grid>
            </Card>

            <Box sx={{ display: 'flex', justifyContent: 'flex-end', my: 4, gap: 3 }}>
                <Button variant="outlined" color="error" onClick={handleBack}>BATALKAN</Button>
                <Button variant="contained" onClick={handleSubmit}>SIMPAN</Button>
            </Box>
        </>
    )
}

export default ProgramAdd