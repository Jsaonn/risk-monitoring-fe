export const DUMMY_PROGRAM = [
    {
        "id": "1",
        "ttc": "BSD",
        "name": "Battery Replacement West",
        "value": "6.7",
        "totalWeek": "12",
        "milestone": [
            {
                "name": "Capex Paper",
                "progress": 100,
                "week": 4,
            },
            {
                "name": "KPAA",
                "progress": 100,
                "week": 2,
            },
            {
                "name": "DP",
                "progress": 100,
                "week": 6,
            },
            {
                "name": "Sourcing",
                "progress": 70,
                "week": 0,
            },
            {
                "name": "OA",
                "progress": 0,
                "week": 0,
            },
            {
                "name": "EP. ID KPAA ID",
                "progress": 0,
                "week": 0,
            },
            {
                "name": "WBS",
                "progress": 0,
                "week": 0,
            },
            {
                "name": "Pre PR",
                "progress": 0,
                "week": 0,
            },
            {
                "name": "PR",
                "progress": 0,
                "week": 0,
            },
            {
                "name": "PO",
                "progress": 0,
                "week": 0,
            },
        ]
    },
    {
        "id": "2",
        "ttc": "BSD",
        "name": "Battery Replacement East",
        "value": "3.2",
        "totalWeek": "12",
        "milestone": [
            {
                "name": "Capex Paper",
                "progress": 100,
                "week": 4,
            },
            {
                "name": "KPAA",
                "progress": 100,
                "week": 2,
            },
            {
                "name": "DP",
                "progress": 100,
                "week": 6,
            },
            {
                "name": "Sourcing",
                "progress": 70,
                "week": 0,
            },
            {
                "name": "OA",
                "progress": 0,
                "week": 0,
            },
            {
                "name": "EP. ID KPAA ID",
                "progress": 0,
                "week": 0,
            },
            {
                "name": "WBS",
                "progress": 0,
                "week": 0,
            },
            {
                "name": "Pre PR",
                "progress": 0,
                "week": 0,
            },
            {
                "name": "PR",
                "progress": 0,
                "week": 0,
            },
            {
                "name": "PO",
                "progress": 0,
                "week": 0,
            },
        ]
    },
    {
        "id": "3",
        "ttc": "BSD",
        "name": "Modernisasi UPS",
        "value": "34.8",
        "totalWeek": "12",
        "milestone": [
            {
                "name": "Capex Paper",
                "progress": 100,
                "week": 4,
            },
            {
                "name": "KPAA",
                "progress": 100,
                "week": 2,
            },
            {
                "name": "DP",
                "progress": 100,
                "week": 6,
            },
            {
                "name": "Sourcing",
                "progress": 70,
                "week": 0,
            },
            {
                "name": "OA",
                "progress": 0,
                "week": 0,
            },
            {
                "name": "EP. ID KPAA ID",
                "progress": 0,
                "week": 0,
            },
            {
                "name": "WBS",
                "progress": 0,
                "week": 0,
            },
            {
                "name": "Pre PR",
                "progress": 0,
                "week": 0,
            },
            {
                "name": "PR",
                "progress": 0,
                "week": 0,
            },
            {
                "name": "PO",
                "progress": 0,
                "week": 0,
            },
        ]
    },
]