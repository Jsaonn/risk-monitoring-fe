// ** React Imports 
import React, { useState, Fragment } from 'react';
import { useRouter } from 'next/router';

// ** MUI Components 
import Box from "@mui/material/Box"
import Table from "@mui/material/Table"
import TableRow from "@mui/material/TableRow"
import TableHead from "@mui/material/TableHead"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import Typography from "@mui/material/Typography"
import IconButton from "@mui/material/IconButton"
import { TablePagination, TableContainer, Collapse, Grid } from "@mui/material"

import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';

import { ModalConfirm } from '@components/modal';

const Row = ({ row, index, onDelete }) => {
    const [open, setOpen] = useState(false)
    const [modalOpen, setModalOpen] = useState(false)
    const [modalData, setModalData] = useState([])

    const router = useRouter();
    const { id, ttc } = router.query

    const handleModalOpen = async (data) => {
        await setModalData(data)
        setModalOpen(true)
    }

    const handleModalClose = () => {
        setModalOpen(false)
    }

    const handleEdit = async (programId) => {
        await router.push({
            pathname: `/dashboard/[id]/program-management/[programId]/edit`,
            query: { 
              id: id,
              programId: programId,
              ttc: ttc
            },
        });
    }

    const handleDelete = (id) => {
        onDelete(id)
        handleModalClose()
    }

    return(
        <Fragment>
            <TableRow
                sx={{
                    background: open ? "rgba(244, 245, 250, 1)" : "#fff",
                }}
            >
                <TableCell sx={{ border: '1px solid #828282' }}>
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowDownIcon /> : <KeyboardArrowRightIcon />}
                    </IconButton>
                </TableCell>
                <TableCell component='th' scope='row' sx={{ border: '1px solid #828282' }}>
                    <Typography variant='body2'>
                        {row.name}
                    </Typography>
                </TableCell>
                <TableCell component='th' scope='row' align='center' sx={{ border: '1px solid #828282' }}>
                    <Typography variant='body2'>
                        {row.value}
                    </Typography>
                </TableCell>
                <TableCell component='th' scope='row' align='center' sx={{ border: '1px solid #828282' }}>
                    <Typography variant='body2'>
                        {row.totalWeek}
                    </Typography>
                </TableCell>
                <TableCell component='th' scope='row' align='center' sx={{ border: '1px solid #828282' }}>
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <IconButton 
                            color="success"
                            onClick={()=>handleEdit(row.id)}
                        >
                            <EditIcon />
                        </IconButton>
                        <IconButton color="error" onClick={()=>handleModalOpen(row)}>
                            <DeleteIcon />
                        </IconButton>
                    </Box>
                </TableCell>
            </TableRow>

            <TableRow sx={{ background: "#fff", border: '1px solid #828282' }}>
                <TableCell colSpan={5} sx={{ px: "0 !important", py: "0 !important" }}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Grid container>
                            <Grid item xs={6}>
                                <Box sx={{ pl: "85px", py: 3.5, borderBottom: "1px solid rgba(58, 53, 65, 0.12)" }}>
                                    <Typography variant='subtitle1' fontWeight={600} textTransform="uppercase">
                                        Milestone
                                    </Typography>
                                </Box>
                            </Grid>
                            <Grid item xs={3}>
                                <Box sx={{ pl: "85px", py: 3.5, borderBottom: "1px solid rgba(58, 53, 65, 0.12)" }}>
                                    <Typography variant='subtitle1' fontWeight={600} textTransform="uppercase" align='center'>
                                        Progress
                                    </Typography>
                                </Box>
                            </Grid>
                            <Grid item xs={3}>
                                <Box sx={{ pl: "85px", py: 3.5, borderBottom: "1px solid rgba(58, 53, 65, 0.12)" }}>
                                    <Typography variant='subtitle1' fontWeight={600} textTransform="uppercase" align='center'>
                                        Week
                                    </Typography>
                                </Box>
                            </Grid>

                            {row.programMilestoneData.map((item) => (
                                <>
                                    <Grid item xs={12} md={6}>
                                        <Box sx={{ pl: "85px", py: 3.5, borderBottom: "1px solid rgba(58, 53, 65, 0.12)" }}>
                                            <Typography variant="body2">{item.name}</Typography>
                                        </Box>
                                    </Grid>
                                    <Grid item xs={12} md={3}>
                                        <Box sx={{ pl: "85px", py: 3.5, borderBottom: "1px solid rgba(58, 53, 65, 0.12)" }}>
                                            <Typography variant="body2" align='center'>{item.progress}%</Typography>
                                        </Box>
                                    </Grid>
                                    <Grid item xs={12} md={3}>
                                        <Box sx={{ pl: "85px", py: 3.5, borderBottom: "1px solid rgba(58, 53, 65, 0.12)" }}>
                                            <Typography variant="body2" align='center'>{item.week}</Typography>
                                        </Box>
                                    </Grid>
                                </>
                            ))}
                        </Grid>
                    </Collapse>
                </TableCell>
            </TableRow>

            <ModalConfirm open={modalOpen} onClose={handleModalClose} data={modalData} onDelete={handleDelete} />
        </Fragment>
    )
}

export const ProgramListTable = ({ data, onDelete }) => {
    const [page, setPage] = useState(0)
    const [rowsPerPage, setRowsPerPage] = useState(10)

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    return (
        <>
            <TableContainer sx={{ mt: 2 }}>
                <Table aria-label="collapsible table">
                    <TableHead sx={{ backgroundColor: '#F9FAFC' }}>
                        <TableRow>
                            <TableCell sx={{ width: "50px", p: 0, pl: "0 !important", border: '1px solid #828282' }} />
                            <TableCell sx={{ minWidth: "400px", fontWeight: 'bold', border: '1px solid #828282' }}>
                                Nama Program
                            </TableCell>
                            <TableCell align="center" sx={{ minWidth: "150px", fontWeight: 'bold', border: '1px solid #828282' }}>
                                Value Program
                            </TableCell>
                            <TableCell align="center" sx={{ minWidth: "100px", fontWeight: 'bold', border: '1px solid #828282' }}>
                                Total Minggu Program
                            </TableCell>
                            <TableCell align="center" sx={{ minWidth: "120px", fontWeight: 'bold', border: '1px solid #828282' }}>
                                Action
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data
                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        .map((row, i) => (
                            <Row
                                key={`row-${i}`}
                                row={row}
                                index={i+1}
                                onDelete={onDelete}
                            />
                        ))}
                    </TableBody>
                </Table>
                <TablePagination
                    component="div"
                    count={data.length}
                    page={page}
                    onPageChange={handleChangePage}
                    rowsPerPage={rowsPerPage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    sx={{ background: '#fff' }}
                />
            </TableContainer>
        </>
    )
}