// ** React Imports 
import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { apiGetAllRiskData, apiDeleteRisk, apiGetAllKeteranganData } from 'lib/axios';

// ** MUI Components 
import { Typography, Button } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';

// ** Components 
import { RISK_MANAGEMENT_DATA } from '@modules/risk-management/components';
import { RiskListTable } from '@modules/risk-management/components/table';


const RiskList = () => {
    const [riskData, setRiskData] = useState([]);
    const [isDataFetched, setIsDataFetched] = useState(false);

    const router = useRouter();
    const { id, ttc } = router.query

    const getRiskData = () => {
        apiGetAllRiskData().then(result => {
            const fetchData = result.data.data.filter(item => item.ttc == id)

            if (fetchData) {
                setRiskData(fetchData)
                setIsDataFetched(true)
            }
        })
    }

    useEffect(() => {
        if (!riskData.length && !isDataFetched) {
            getRiskData()
        }
    }, [riskData, isDataFetched])

    const handleAdd = async () => {
        const getKeteranganData = async () => {
            apiGetAllKeteranganData().then(result => {
                if (!result.data.data.length) {
                    alert('Risk tidak dapat ditambah karena tipe risk belum dibuat!')
                    return
                } else {
                    router.push({
                        pathname: `/dashboard/[id]/risk-management/add`,
                        query: { 
                          id: id,
                          ttc: ttc
                        },
                    });
                }
            })
        }
        getKeteranganData()
    }

    const handleDelete = (id) => {
        apiDeleteRisk(id)
        setRiskData([])
        setIsDataFetched(false)
    }

    return(
        <>
            <Typography 
                variant="h3" 
                color="#E51C28"
                fontWeight={700}
                sx={{ 
                    mt: 2
                }}
            >
                TTC {ttc} - Risk Register Update
            </Typography>

            <Button 
                variant="contained" 
                endIcon={<AddIcon />} 
                color="success"
                onClick={handleAdd}
            >
                Tambah Risk
            </Button>

            <RiskListTable data={riskData} onDelete={handleDelete} />
        </>
    )
}

export default RiskList