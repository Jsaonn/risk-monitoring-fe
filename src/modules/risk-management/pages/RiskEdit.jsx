// ** React Imports 
import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';

// ** MUI Components 
import { 
    Box,
    Button,
    Card,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    TextField,
    Typography,
    OutlinedInput
} from '@mui/material';

import { apiGetRiskById, apiUpdateRisk, apiGetAllKeteranganData } from 'lib/axios';
import { levelOfRiskCalc } from 'lib';


const RiskEdit = () => {
    const [keteranganOption, setKeteranganOption] = useState([])
    const [name, setName] = useState("")
    const [keterangan, setKeterangan] = useState("")
    const [likelihood, setLikelihood] = useState(0)
    const [consequence, setConsequence] = useState(0)
    const [level, setLevel] = useState("VL")
    const [impact, setImpact] = useState("")

    const router = useRouter();
    const { id, ttc, riskId } = router.query

    useEffect(() => {
        const getRiskData = async () => {
            apiGetRiskById(riskId).then(result => {
                setName(result.data.data.name)
                setKeterangan(result.data.data.keterangan)
                setLikelihood(result.data.data.likelihood)
                setConsequence(result.data.data.consequence)
                setLevel(result.data.data.level)
                setImpact(result.data.data.impact)
            })
        }

        const getKeteranganData = async () => {
            apiGetAllKeteranganData().then(result => {
                setKeteranganOption(result.data.data)
            })
        }

        if (riskId) {
            getRiskData()
        }

        if (!keteranganOption.length) {
            getKeteranganData()
        }
    }, [riskId, keteranganOption])

    const handleName = (event) => {
        let value = event.target.value
        setName(value)
    }

    const handleKeterangan = (event) => {
        let value = event.target.value
        setKeterangan(value)
    }

    const handleLikelihood = (event) => {
        let value = event.target.value
        setLikelihood(value)

        if (value !== 0 && consequence !== 0) {
            const levelOfRisk = levelOfRiskCalc(value, consequence)
            setLevel(levelOfRisk)
        }
    }

    const handleConsequence = (event) => {
        let value = event.target.value
        setConsequence(value)

        if (value !== 0 && likelihood !== 0) {
            const levelOfRisk = levelOfRiskCalc(likelihood, value)
            setLevel(levelOfRisk)
        }
    }

    const handleImpact = (event) => {
        let value = event.target.value
        setImpact(value)
    }

    const handleSubmit = (event) => {
        if (!name || !keterangan || !likelihood || !consequence || !level || !impact) {
            alert('Terdapat field yang belum diisi!')
            return
        }

        event.preventDefault()
        const data = {
            "ttc": id,
            "ttcName": ttc,
            "name": name,
            "keterangan": keterangan,
            "likelihood": likelihood,
            "consequence": consequence,
            "level": level,
            "impact": impact
        }

        apiUpdateRisk(data, riskId).then(
            router.back()
        )
    }

    const handleBack = async () => {
        await router.back();
    }

    return(
        <>
            <Typography 
                variant="h3" 
                color="#E51C28"
                fontWeight={700}
                sx={{ 
                    mt: 2
                }}
            >
                TTC {ttc} - Edit Risk
            </Typography>

            <Card sx={{ p: 5 }}>
                <Typography variant="h5">Masukkan Data Risiko</Typography>
                <Grid container rowSpacing={2} columnSpacing={{ md: 6 }} sx={{ my: 1 }}>
                    <Grid item xs={12}>
                        <TextField 
                            fullWidth 
                            id="outlined-basic" 
                            label="Nama TTC" 
                            variant="outlined" 
                            disabled 
                            value={ttc} 
                            InputLabelProps={{ shrink: true }} 
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <TextField 
                            fullWidth 
                            id="outlined-basic" 
                            label="Nama Risiko" 
                            variant="outlined" 
                            required
                            onChange={handleName}
                            value={name}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <FormControl fullWidth required>
                            <InputLabel id="select-class-label">Keterangan</InputLabel>
                            <Select 
                                fullWidth 
                                labelId="select-class-label"
                                id="select-class"
                                value={keterangan}
                                onChange={handleKeterangan}
                                input={<OutlinedInput label="keterangan" />}
                            >
                                {keteranganOption && keteranganOption.map((item) => (
                                    <MenuItem
                                        key={item.id}
                                        value={item.name}
                                    >
                                        {item.name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>

                    <Grid item xs={6}>
                        <FormControl fullWidth required>
                            <InputLabel id="select-class-label">Likelihood</InputLabel>
                            <Select fullWidth labelId="select-class-label" id="select-class" label="Likelihood" value={likelihood} onChange={handleLikelihood}>
                                <MenuItem value={1}>1</MenuItem>
                                <MenuItem value={2}>2</MenuItem>
                                <MenuItem value={3}>3</MenuItem>
                                <MenuItem value={4}>4</MenuItem>
                                <MenuItem value={5}>5</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>

                    <Grid item xs={6}>
                        <FormControl fullWidth required>
                            <InputLabel id="select-class-label">Consequences</InputLabel>
                            <Select fullWidth labelId="select-class-label" id="select-class" label="Consequences" value={consequence} onChange={handleConsequence}>
                                <MenuItem value={1}>1</MenuItem>
                                <MenuItem value={2}>2</MenuItem>
                                <MenuItem value={3}>3</MenuItem>
                                <MenuItem value={4}>4</MenuItem>
                                <MenuItem value={5}>5</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>

                    <Grid item xs={12}>
                        <TextField fullWidth id="outlined-basic" label="Level of Risk" variant="outlined" required disabled value={level} InputLabelProps={{ shrink: true }} />
                    </Grid>

                    <Grid item xs={12}>
                        <FormControl fullWidth required>
                            <InputLabel id="select-class-label">Impact-Effort</InputLabel>
                            <Select fullWidth labelId="select-class-label" id="select-class" label="Impact-Effort" value={impact} onChange={handleImpact}>
                                <MenuItem value="LIHE">Low Effort - High Impact</MenuItem>
                                <MenuItem value="HIHE">High Effort - High Impact</MenuItem>
                                <MenuItem value="LILE">Low Effort - Low Impact</MenuItem>
                                <MenuItem value="HILE">High Effort - Low Impact</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                </Grid>
            </Card>

            <Box sx={{ display: 'flex', justifyContent: 'flex-end', my: 4, gap: 3 }}>
                <Button variant="outlined" color="error" onClick={handleBack}>BATALKAN</Button>
                <Button variant="contained" onClick={handleSubmit}>SIMPAN</Button>
            </Box>
        </>
    )
}

export default RiskEdit