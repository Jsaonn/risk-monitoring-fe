// ** React Imports 
import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { apiGetAllMilestoneData, apiDeleteMilestone, apiGetAllRolesData } from 'lib/axios';

// ** MUI Components 
import { Box, Typography, Grid, Button } from '@mui/material';

// ** Components 
import { TTC_MILESTONE_DATA } from '@modules/milestone-config/components';
import { MilestoneListTable } from '@modules/milestone-config/components/table';

import AddIcon from '@mui/icons-material/Add';

const MilestoneList = () => {
    const [milestoneData, setMilestoneData] = useState([])
    const [isDataFetched, setIsDataFetched] = useState(false);

    const router = useRouter();
    const { id, ttc } = router.query

    const getMilestoneData = () => {
        if (id) {
            apiGetAllMilestoneData().then(result => {
                const fetchData = result.data.data.filter(item => item.ttc == id)
                
                if (fetchData) {
                    setMilestoneData(fetchData)
                    setIsDataFetched(true)
                }
            })
        }
    }

    useEffect(() => {
        if (!milestoneData.length && !isDataFetched) {
            getMilestoneData()
        }
    }, [milestoneData, isDataFetched, id])

    const handleAdd = async () => {
        const getRolesData = async () => {
            apiGetAllRolesData().then(result => {
                if (!result.data.data.length) {
                    alert('Milestone tidak dapat ditambah karena roles user belum dibuat!')
                    return
                } else {
                    router.push({
                        pathname: `/dashboard/[id]/milestone-config/add`,
                        query: { 
                          id: id,
                          ttc: ttc
                        },
                    });
                }
            })
        }
        getRolesData()
    }

    const handleDelete = (id) => {
        apiDeleteMilestone(id)
        setMilestoneData([])
        setIsDataFetched(false)
    }

    return(
        <>
            <Typography 
                variant="h3" 
                color="#E51C28"
                fontWeight={700}
                sx={{ 
                    mt: 2
                }}
            >
                TTC {ttc} - Milestone Configuration
            </Typography>

            <Button 
                variant="contained" 
                endIcon={<AddIcon />} 
                color="success"
                onClick={handleAdd}
            >
                Tambah Milestone
            </Button>

            {milestoneData && 
                <MilestoneListTable data={milestoneData} onDelete={handleDelete} />
            }
        </>
    )
}

export default MilestoneList