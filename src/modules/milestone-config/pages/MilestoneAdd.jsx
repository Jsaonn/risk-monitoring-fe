// ** React Imports 
import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';

// ** MUI Components 
import { 
    Box,
    Button,
    Card,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    TextField,
    Typography,
    OutlinedInput
} from '@mui/material';

import { apiGetAllRolesData, apiCreateMilestone } from 'lib/axios';

const MilestoneAdd = () => {
    const [rolesOption, setRolesOption] = useState([])
    const [name, setName] = useState("")
    const [abbr, setAbbr] = useState("")
    const [roles, setRoles] = useState([])

    const router = useRouter();
    const { id, ttc } = router.query;

    useEffect(() => {
        const getRolesData = async () => {
            apiGetAllRolesData().then(result => {
                setRolesOption(result.data.data)
            })
        }

        if (!rolesOption.length) {
            getRolesData()
        }

    }, [rolesOption])

    const handleName = (event) => {
        let value = event.target.value
        setName(value)
    }

    const handleAbbr = (event) => {
        let value = event.target.value
        setAbbr(value)
    }

    const handleChange = (event) => {
        const {
          target: { value },
        } = event;
        setRoles(
          typeof value === 'string' ? value.split(',') : value,
        );
    };

    const handleSubmit = (event) => {
        if (!name || !abbr || !roles.length) {
            alert('Terdapat field yang belum diisi!')
            return
        }

        event.preventDefault()
        const data = {
            "ttc": id,
            "name": name,
            "abbr": abbr,
            "roles": roles
        }
        
        apiCreateMilestone(data).then(
            router.back()
        )
    }

    const handleBack = async () => {
        await router.back();
    }

    return(
        <>
            <Typography 
                variant="h3" 
                color="#E51C28"
                fontWeight={700}
                sx={{ 
                    mt: 2
                }}
            >
                TTC {ttc} - Add Milestone
            </Typography>

            <Card sx={{ p: 5 }}>
                <Typography variant="h5">Masukkan Data Milestone</Typography>
                <Grid container rowSpacing={2} columnSpacing={{ md: 6 }} sx={{ my: 1 }}>
                    <Grid item xs={12}>
                        <TextField 
                            fullWidth 
                            id="outlined-basic" 
                            label="Nama TTC" 
                            variant="outlined" 
                            disabled 
                            value={ttc} 
                            InputLabelProps={{ shrink: true }} 
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <TextField 
                            fullWidth 
                            id="outlined-basic" 
                            label="Nama Milestone" 
                            variant="outlined" 
                            required
                            onChange={handleName}
                            value={name}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <TextField 
                            fullWidth 
                            id="outlined-basic" 
                            label="Singkatan Milestone" 
                            variant="outlined" 
                            required
                            onChange={handleAbbr}
                            value={abbr}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <FormControl fullWidth required>
                            <InputLabel id="select-class-label">Roles</InputLabel>
                            <Select 
                                fullWidth 
                                multiple
                                labelId="select-class-label"
                                id="select-class"
                                value={roles}
                                onChange={handleChange}
                                input={<OutlinedInput label="Roles" />}
                            >
                                {rolesOption && rolesOption.map((role) => (
                                    <MenuItem
                                        key={role.id}
                                        value={role.name}
                                    >
                                        {role.name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                </Grid>
            </Card>

            <Box sx={{ display: 'flex', justifyContent: 'flex-end', my: 4, gap: 3 }}>
                <Button variant="outlined" color="error" onClick={handleBack}>BATALKAN</Button>
                <Button variant="contained" onClick={handleSubmit}>SIMPAN</Button>
            </Box>
        </>
    )
}

export default MilestoneAdd