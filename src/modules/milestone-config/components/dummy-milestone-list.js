export const TTC_MILESTONE_DATA = [
    {
        "id": "1",
        "title": "BSD",
        "milestone": [
            {
                "id": "1",
                "name": "Capex Paper",
                "abbr": "CP",
                "roles": ["Business User"]
            },
            {
                "id": "2",
                "name": "KPAA",
                "abbr": "KPAA",
                "roles": ["FBP"]
            },
            {
                "id": "3",
                "name": "DP",
                "abbr": "DP",
                "roles": ["Business User", "Procurement"]
            },
            {
                "id": "4",
                "name": "Sourcing",
                "abbr": "SOU.",
                "roles": ["Procurement"]
            },
            {
                "id": "5",
                "name": "OA",
                "abbr": "OA",
                "roles": ["Procurement"]
            },
            {
                "id": "6",
                "name": "EP. ID KPAA ID",
                "abbr": "EP. ID KPAA ID",
                "roles": ["Business User", "FBP"]
            },
            {
                "id": "7",
                "name": "WBS",
                "abbr": "WBS",
                "roles": ["Business User"]
            },
            {
                "id": "8",
                "name": "Pre PR",
                "abbr": "Pre PR",
                "roles": ["Business User", "FBP"]
            },
            {
                "id": "9",
                "name": "PR",
                "abbr": "PR",
                "roles": ["Business User"]
            },
            {
                "id": "10",
                "name": "PO",
                "abbr": "PO",
                "roles": ["Procurement"]
            },
        ],
    },
    {
        "id": "2",
        "title": "TBS",
        "milestone": [],
    },
    {
        "id": "3",
        "title": "BUARAN",
        "milestone": [],
    },
    {
        "id": "4",
        "title": "ARIFIN AHMAD",
        "milestone": [],
    },
    {
        "id": "5",
        "title": "SOLO BARU",
        "milestone": [],
    },
    {
        "id": "6",
        "title": "SUDIANG",
        "milestone": [],
    },
    {
        "id": "7",
        "title": "GAYUNGAN",
        "milestone": [],
    },
]