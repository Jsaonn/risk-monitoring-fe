// ** React Imports 
import React, { useState, Fragment } from 'react';
import { useRouter } from 'next/router';

// ** MUI Components 
import Box from "@mui/material/Box"
import Table from "@mui/material/Table"
import TableRow from "@mui/material/TableRow"
import TableHead from "@mui/material/TableHead"
import TableBody from "@mui/material/TableBody"
import TableCell from "@mui/material/TableCell"
import Typography from "@mui/material/Typography"
import IconButton from "@mui/material/IconButton"
import { TablePagination, TableContainer } from "@mui/material"
import { styled } from "@mui/material/styles"
import Chip from '@mui/material/Chip';

import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';

import { ModalConfirm } from '@components/modal';

const Row = ({ row, index, onDelete }) => {
    const [modalOpen, setModalOpen] = useState(false)
    const [modalData, setModalData] = useState([])

    const router = useRouter();
    const { id, ttc } = router.query

    const handleModalOpen = async (data) => {
        await setModalData(data)
        setModalOpen(true)
    }

    const handleModalClose = () => {
        setModalOpen(false)
    }

    const handleEdit = async (milestoneId) => {
        await router.push({
            pathname: `/dashboard/[id]/milestone-config/[milestoneId]/edit`,
            query: { 
              id: id,
              milestoneId: milestoneId,
              ttc: ttc
            },
        });
    }

    const handleDelete = (id) => {
        onDelete(id)
        handleModalClose()
    }

    return(
        <Fragment>
            <TableRow sx={{ background: '#fff' }}>
                <TableCell component='th' scope='row' align='center' sx={{ border: '1px solid #828282' }}>
                    <Typography variant='body2'>
                        {index}
                    </Typography>
                </TableCell>
                <TableCell component='th' scope='row' sx={{ border: '1px solid #828282' }}>
                    <Typography variant='body2'>
                        {row.name}
                    </Typography>
                </TableCell>
                <TableCell component='th' scope='row' align='center' sx={{ border: '1px solid #828282' }}>
                    <Typography variant='body2'>
                        {row.abbr}
                    </Typography>
                </TableCell>
                <TableCell component='th' scope='row' align='center' sx={{ border: '1px solid #828282' }}>
                    {row.milestoneRolesData.map((role, idx) => (
                        <Typography variant='body2' key={idx}>
                            {role === row.milestoneRolesData[row.milestoneRolesData.length-1] ? role.name : `${role.name}, `}
                        </Typography>
                    ))}
                </TableCell>
                <TableCell component='th' scope='row' align='center' sx={{ border: '1px solid #828282' }}>
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <IconButton 
                            color="success"
                            onClick={()=>handleEdit(row.id)}
                        >
                            <EditIcon />
                        </IconButton>
                        <IconButton color="error" onClick={()=>handleModalOpen(row)}>
                            <DeleteIcon />
                        </IconButton>
                    </Box>
                </TableCell>
            </TableRow>

            <ModalConfirm open={modalOpen} onClose={handleModalClose} data={modalData} onDelete={handleDelete} />
        </Fragment>
    )
}

export const MilestoneListTable = ({ data, onDelete }) => {
    const [page, setPage] = useState(0)
    const [rowsPerPage, setRowsPerPage] = useState(10)

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    return (
        <>
            <TableContainer sx={{ mt: 2 }}>
                <Table aria-label="collapsible table">
                    <TableHead sx={{ backgroundColor: '#F9FAFC' }}>
                        <TableRow>
                            <TableCell align="center" sx={{ minWidth: "50px", fontWeight: 'bold', border: '1px solid #828282' }}>
                                No
                            </TableCell>
                            <TableCell sx={{ minWidth: "400px", fontWeight: 'bold', border: '1px solid #828282' }}>
                                Nama Milestone
                            </TableCell>
                            <TableCell align="center" sx={{ minWidth: "150px", fontWeight: 'bold', border: '1px solid #828282' }}>
                                Singkatan Milestone
                            </TableCell>
                            <TableCell align="center" sx={{ minWidth: "100px", fontWeight: 'bold', border: '1px solid #828282' }}>
                                Roles
                            </TableCell>
                            <TableCell align="center" sx={{ minWidth: "120px", fontWeight: 'bold', border: '1px solid #828282' }}>
                                Action
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data
                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        .map((row, i) => (
                            <Row
                                key={`row-${i}`}
                                row={row}
                                index={i+1}
                                onDelete={onDelete}
                            />
                        ))}
                    </TableBody>
                </Table>
                <TablePagination
                    component="div"
                    count={data.length}
                    page={page}
                    onPageChange={handleChangePage}
                    rowsPerPage={rowsPerPage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    sx={{ background: '#fff', border: '1px solid #828282' }}
                />
            </TableContainer>
        </>
    )
}