// ** React Imports 
import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';

// ** MUI Components 
import { Box, Typography } from '@mui/material';

// ** Components 
import { ActionWithIconCard, DETAIL_CONTENT, DETAIL_CONTENT_2 } from '@modules/dashboard/components';

const DashboardDetailPage = () => {
    const router = useRouter()
    const { id, ttc } = router.query

    return (
      <>
        <Typography 
            variant="h3" 
            color="#E51C28"
            fontWeight={700}
            sx={{ 
                mt: 2
            }}
        >
            TTC {ttc}
        </Typography>

        <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexWrap: 'wrap' ,gap: 5, mt: 10 }}>
            {
                DETAIL_CONTENT.map((item, index) => (
                    <ActionWithIconCard key={index} icon={item.icon} title={item.title} url={item.url} id={id} ttc={ttc} />
                ))
            }
        </Box>

        <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexWrap: 'wrap' ,gap: 5, mt: 10 }}>
            {
                DETAIL_CONTENT_2.map((item, index) => (
                    <ActionWithIconCard key={index} icon={item.icon} title={item.title} url={item.url} id={id} ttc={ttc} />
                ))
            }
        </Box>
      </>
    );
  }
  
export default DashboardDetailPage;