// ** React Imports 
import React, { useState, useEffect } from 'react';
import { apiGetAllTtcData } from 'lib/axios';

// ** MUI Components 
import { Box } from '@mui/material';

// ** Components 
import { TitleCard } from '@modules/dashboard/components';

const DashboardPage = () => {
  const [ttcData, setTtcData] = useState([]);
  const [isDataFetched, setIsDataFetched] = useState(false);

  const getTtcData = () => {
    apiGetAllTtcData().then(result => {
      setTtcData(result.data.data)
      setIsDataFetched(true)
    })
  }

  useEffect(() => {
    if (!ttcData.length && !isDataFetched) {
      getTtcData()
    }
  }, [ttcData, isDataFetched])

  return (
    <>
      <Box sx={{ display: 'flex', justifyContent: 'center', gap: 5, alignItems: 'center', flexWrap: 'wrap' }}>
        {ttcData &&
          ttcData.map((item, index) => (
            <TitleCard key={index} title={item.name} id={item.id} />
          ))
        }
      </Box>
    </>
  );
}

export default DashboardPage;