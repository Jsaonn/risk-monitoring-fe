// ** React Imports 
import React from 'react';
import { useRouter } from "next/router";

// ** MUI Components 
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';

export const TitleCard = ({ title, id }) => {
  const router = useRouter();

  const handleDetail = async () => {
    await router.push({
      pathname: `/dashboard/[id]/detail`,
      query: { 
        id: id,
        ttc: title
      },
    });
  }

  return (
    <>
      <Card sx={{ maxWidth: 250 }}>
        <CardActionArea onClick={handleDetail}>
          <CardMedia
            component="img"
            height="250"
            image="/images/pages/Picture1.png"
            alt="green iguana"
            sx={{ p: '10px 10px 0px 10px', borderRadius: 2 }}
          />
          <CardContent>
            <Typography variant="h5" component="div" align='center' fontWeight={700}>
              {title}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </>
  )
}
