// ** React Imports 
import React from 'react';
import { useRouter } from "next/router";

// ** MUI Components 
import { Typography } from '@mui/material';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { CardActionArea } from '@mui/material';

export const ActionWithIconCard = ({ icon, title, url, id, ttc }) => {
    const router = useRouter();
  
    const handleNext = async () => {
      await router.push({
        pathname: `/dashboard/[id]/${url}`,
        query: { 
          id: id,
          ttc: ttc
        },
      });
    }
  
    return (
      <>
        <Card sx={{ width: 250 }}>
            <CardActionArea sx={{ p: 4, height: 200 }} onClick={handleNext}>
                <CardContent sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', gap: 2 }}>
                    {icon}
                    <Typography variant="h5" component="div" align='center' fontWeight={700}>
                        {title}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
      </>
    )
  }