import ControlPointIcon from '@mui/icons-material/ControlPoint';
import ConstructionIcon from '@mui/icons-material/Construction';
import HourglassBottomIcon from '@mui/icons-material/HourglassBottom';
import HourglassDisabledIcon from '@mui/icons-material/HourglassDisabled';
import TerminalIcon from '@mui/icons-material/Terminal';
import DisplaySettingsIcon from '@mui/icons-material/DisplaySettings';
import { grey } from '@mui/material/colors';

export const DUMMY_TTC = [
    {
        "id": "1",
        "title": "BSD"
    },
    {
        "id": "2",
        "title": "TBS"
    },
    {
        "id": "3",
        "title": "BUARAN"
    },
    {
        "id": "4",
        "title": "ARIFIN AHMAD"
    },
    {
        "id": "5",
        "title": "SOLO BARU"
    },
    {
        "id": "6",
        "title": "SUDIANG"
    },
    {
        "id": "7",
        "title": "GAYUNGAN"
    },
]

export const DETAIL_CONTENT = [
    {
        "title": "Risk Register",
        "url": "risk-assessment",
        "icon": <ControlPointIcon fontSize='large' sx={{ color: grey[900] }} />,
    },
    {
        "title": "Mitigation Program Progress",
        "url": "progress",
        "icon": <ConstructionIcon fontSize='large' sx={{ color: grey[900] }} />,
    },
    {
        "title": "Mitigation Program Milestone",
        "url": "milestone",
        "icon": <HourglassBottomIcon fontSize='large' sx={{ color: grey[900] }} />,
    },
]

export const DETAIL_CONTENT_2 = [
    {
        "title": "Risk Register Update",
        "url": "risk-management",
        "icon": <DisplaySettingsIcon fontSize='large' sx={{ color: grey[900] }} />,
    },
    {
        "title": "Mitigation Program Update",
        "url": "program-management",
        "icon": <TerminalIcon fontSize='large' sx={{ color: grey[900] }} />,
    },
    {
        "title": "Milestone Update",
        "url": "milestone-config",
        "icon": <HourglassDisabledIcon fontSize='large' sx={{ color: grey[900] }} />,
    },
]