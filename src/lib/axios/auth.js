import { axiosClient } from "./__axios__"

export const apiLogin = (data) => {
    return axiosClient.post('/api/login', data)
}

export const apiRegister = (data) => {
    return axiosClient.post('/api/register', data)
}

export const apiLogout = (email) => {
    return axiosClient.get(`/api/logout/${email}`)
}