import { axiosClient } from "./__axios__"

export const apiGetAllMilestoneData = () => {
    return axiosClient.get('/milestone/')
}

export const apiGetMilestoneById = (id) => {
    return axiosClient.get(`/milestone/${id}`)
}

export const apiCreateMilestone = (data) => {
    return axiosClient.post('/milestone/create', data)
}

export const apiUpdateMilestone = (data, id) => {
    return axiosClient.put(`/milestone/update/${id}`, data)
}

export const apiDeleteMilestone = (id) => {
    return axiosClient.delete(`/milestone/delete/${id}`)
}