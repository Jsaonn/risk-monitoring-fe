import { axiosClient } from "./__axios__"

export const apiGetAllKeteranganData = () => {
    return axiosClient.get('/keterangan/')
}

export const apiGetKeteranganById = (id) => {
    return axiosClient.get(`/keterangan/${id}`)
}

export const apiCreateKeterangan = (data) => {
    return axiosClient.post('/keterangan/create', data)
}

export const apiUpdateKeterangan = (data, id) => {
    return axiosClient.put(`/keterangan/update/${id}`, data)
}

export const apiDeleteKeterangan = (id) => {
    return axiosClient.delete(`/keterangan/delete/${id}`)
}