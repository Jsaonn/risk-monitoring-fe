import { axiosClient } from "./__axios__"

export const apiGetAllRiskData = () => {
    return axiosClient.get('/risk/')
}

export const apiGetRiskById = (id) => {
    return axiosClient.get(`/risk/${id}`)
}

export const apiCreateRisk = (data) => {
    return axiosClient.post('/risk/create', data)
}

export const apiUpdateRisk = (data, id) => {
    return axiosClient.put(`/risk/update/${id}`, data)
}

export const apiDeleteRisk = (id) => {
    return axiosClient.delete(`/risk/delete/${id}`)
}