import { axiosClient } from "./__axios__"

export const apiGetAllTtcData = () => {
    return axiosClient.get('/ttc/')
}

export const apiGetTtcById = (id) => {
    return axiosClient.get(`/ttc/${id}`)
}

export const apiCreateTtc = (data) => {
    return axiosClient.post('/ttc/create', data)
}

export const apiUpdateTtc = (data, id) => {
    return axiosClient.put(`/ttc/update/${id}`, data)
}

export const apiDeleteTtc = (id) => {
    return axiosClient.delete(`/ttc/delete/${id}`)
}