import { axiosClient } from "./__axios__"

export const apiGetAllProgramData = () => {
    return axiosClient.get('/program/')
}

export const apiGetProgramById = (id) => {
    return axiosClient.get(`/program/${id}`)
}

export const apiCreateProgram = (data) => {
    return axiosClient.post('/program/create', data)
}

export const apiUpdateProgram = (data, id) => {
    return axiosClient.put(`/program/update/${id}`, data)
}

export const apiDeleteProgram = (id) => {
    return axiosClient.delete(`/program/delete/${id}`)
}