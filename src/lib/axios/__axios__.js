import axios from "axios";
const API_URL = "http://127.0.0.1:8000/"

export default {API_URL}

export const axiosClient = axios.create({
    baseURL: `${API_URL}`,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
})
