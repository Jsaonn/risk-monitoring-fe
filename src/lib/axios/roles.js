import { axiosClient } from "./__axios__"

export const apiGetAllRolesData = () => {
    return axiosClient.get('/roles/')
}

export const apiGetRoleById = (id) => {
    return axiosClient.get(`/roles/${id}`)
}

export const apiCreateRoles = (data) => {
    return axiosClient.post('/roles/create', data)
}

export const apiUpdateRoles = (data, id) => {
    return axiosClient.put(`/roles/update/${id}`, data)
}

export const apiDeleteRoles = (id) => {
    return axiosClient.delete(`/roles/delete/${id}`)
}