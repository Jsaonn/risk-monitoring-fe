import { format } from "date-fns"
import { id } from "date-fns/locale"

export const levelOfRiskCalc = (likelihood, consequence) => {
    let result;

    switch (consequence) {
        case 1:
            switch (likelihood) {
                case 1: result = "VL"; break;
                case 2:
                case 3:
                case 4: result = "L"; break;
                case 5: result = "M"; break;
            }
            break;
        case 2:
            switch (likelihood) {
                case 1:
                case 2:
                case 3: result = "L"; break;
                case 4: result = "M"; break;
                case 5: result = "H"; break;
            }
            break;
        case 3: 
            switch (likelihood) {
                case 1:
                case 2:
                case 3: result = "M"; break;
                case 4:
                case 5: result = "H"; break;
            }
            break;
        case 4:
            switch (likelihood) {
                case 1:
                case 2:
                case 3: result = "H"; break;
                case 4:
                case 5: result = "VH"; break;
            }
            break;
        case 5:
            result = "VH"; break;
    }

    return result;
}

// https://stackoverflow.com/questions/36734201/how-to-convert-numbers-to-million-in-javascript
export const numberToCurrency = (labelValue) => {
    // Nine Zeroes for Billions
    return Math.abs(Number(labelValue)) >= 1.0e+9

    ? (Math.abs(Number(labelValue)) / 1.0e+9).toFixed(2) + "B"
    // Six Zeroes for Millions 
    : Math.abs(Number(labelValue)) >= 1.0e+6

    ? (Math.abs(Number(labelValue)) / 1.0e+6).toFixed(2) + "M"
    // Three Zeroes for Thousands
    : Math.abs(Number(labelValue)) >= 1.0e+3

    ? (Math.abs(Number(labelValue)) / 1.0e+3).toFixed(2) + "K"

    : Math.abs(Number(labelValue));
}

export const getAverageProgress = (programData, milestoneLength) => {
    let result = []
    let divider = 0
    let percentage = []

    // get divider
    programData.map(program => {
        divider += program.value
    })

    // get percentage per program for overall progress
    programData.map(program => {
        percentage.push((program.value/divider*100).toFixed(2))
    })

    // get progress value
    for (let i=0; i<milestoneLength; i++) {
        let temp = 0
        for (let j=0; j<programData.length; j++) {
            temp += programData[j].programMilestoneData[i].progress * percentage[j]
        }
        result.push((temp/100).toFixed(0))
    }

    return result
}

export function dateFormatter(date, dateFormat) {
    return format(date, dateFormat, { locale: id })
}

/**
 * https://stackoverflow.com/questions/2483719/get-weeks-in-month-through-javascript
 * Returns number of weeks
 *
 * @param {Number} year - full year (2018)
 * @param {Number} month - zero-based month index (0-11)
 * @param {Boolean} fromMonday - false if weeks start from Sunday, true - from Monday.
 * @returns {number}
 */
export const weeksInMonth = (year, month, fromMonday = false) => {
    const first = new Date(year, month, 1);
    const last  = new Date(year, month + 1, 0);
    let dayOfWeek = first.getDay();
    if (fromMonday && dayOfWeek === 0) dayOfWeek = 7;
    let days = dayOfWeek + last.getDate();
    if (fromMonday) days -= 1;
    return Math.ceil(days / 7);
}

export const getWeekMilestones = (programData) => {
    let lowestDate;
    if (programData.length === 1) {
        lowestDate = programData[0].createdAt
    } else if (programData.length > 1) {
        for (let i=0; i<programData.length-1; i++) {
            if (programData[i].createdAt < programData[i+1].createdAt) {
                lowestDate = programData[i].createdAt
            } else {
                lowestDate = programData[i+1].createdAt
            }
        }
    }
    const newLowDate = new Date(lowestDate)
    let header = [{
        "name": dateFormatter(newLowDate, 'MMM-yyyy'),
        "weekCount": weeksInMonth(newLowDate.getFullYear(), newLowDate.getMonth())
    }]
    let skipWeek = []
    for (let i=1; i<6; i++) {
        const newDate = new Date(newLowDate.setMonth(newLowDate.getMonth()+1))
        header.push({
            "name": dateFormatter(newDate, 'MMM-yyyy'),
            "weekCount": weeksInMonth(newDate.getFullYear(), newDate.getMonth())-1
        })
    }
    programData.map(item => {
        const days = getDateInWeek(item.createdAt)
        const first = days[0].split('-')[2]
        skipWeek.push(
            first < 7 ? 0 :
            7 <= first && first < 14 ? 1 :
            14 <= first && first < 21 ? 2 :
            first >= 21 ? 3 : 4
        )
    })

    return { header, skipWeek }
}

export const getDateInWeek = (day) => {
    let curr = new Date(day)
    let week = []

    for (let i = 1; i <= 7; i++) {
        let first = curr.getDate() - curr.getDay() + i 
        let day = new Date(curr.setDate(first)).toISOString().slice(0, 10)
        week.push(day)
    }

    return week
}