// ** React Imports 
import React, { useState } from 'react';
import { useRouter } from 'next/router';
import Cookies from "js-cookie"

// ** MUI Components 
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';

// ** MUI Icons 
import CreateIcon from '@mui/icons-material/Create';
import ApartmentIcon from '@mui/icons-material/Apartment';
import WarningIcon from '@mui/icons-material/Warning';
import SettingsIcon from '@mui/icons-material/Settings';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import PersonIcon from '@mui/icons-material/Person';
import DomainAddIcon from '@mui/icons-material/DomainAdd';
import SpeakerNotesIcon from '@mui/icons-material/SpeakerNotes';
import LogoutIcon from '@mui/icons-material/Logout';

const SidebarContent = [
    {
        "title": "List of TTC",
        "icon": <ApartmentIcon />,
        "url": "/dashboard",
    },
    // {
    //     "title": "Support Needed",
    //     "icon": <WarningIcon />,
    //     "url": "/dashboard",
    // },
]

const SystemConfContent = [
    {
        "title": "TTC Configuration",
        "icon": <DomainAddIcon />,
        "url": "/system-config/ttc",
    },
    {
        "title": "User Roles",
        "icon": <PersonIcon />,
        "url": "/system-config/user-roles",
    },
    {
        "title": "Risk Register Configuration",
        "icon": <SpeakerNotesIcon />,
        "url": "/system-config/keterangan",
    },
]

const drawerWidth = 240;

const Layout = ({ children }) => {
  const router = useRouter();
  const [open, setOpen] = useState(true)

  const handleClick = () => {
    setOpen(!open);
  };

  const handleLogout = () => {
    Cookies.remove('token')
    Cookies.remove('email')
    Cookies.remove('role')
    router.push('/')
  }

  return (
    <Box sx={{ display: 'flex' }}>
        <CssBaseline />
        <AppBar
            position="fixed"
            sx={{ width: `calc(100% - ${drawerWidth}px)`, ml: `${drawerWidth}px` }}
        >
            <Toolbar sx={{ backgroundColor: '#A40000' }}>
                <Box sx={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                    <Typography variant="h6" noWrap component="div">
                        TTC Risk Management Monitoring
                    </Typography>
                    {/* <Box sx={{ display: 'flex', gap: 1, alignItems: 'center' }}>
                        <CreateIcon fontSize='16px' />
                        <Typography variant="subtitle2" fontWeight={500}>Last Update,</Typography>
                        <Typography variant="subtitle2">Wednesday, 26 Oct 2022</Typography>
                    </Box> */}
                </Box>
            </Toolbar>
        </AppBar>
        <Drawer
            sx={{
                width: drawerWidth,
                flexShrink: 0,
                '& .MuiDrawer-paper': {
                    width: drawerWidth,
                    boxSizing: 'border-box',
                },
            }}
            variant="permanent"
            anchor="left"
        >
            <Toolbar>
                <img src="/images/logos/image 4.png" width={200} />
            </Toolbar>
            <Divider />
            <List>
                {
                    SidebarContent.map((item, index) => (
                        <ListItem key={index} disablePadding>
                            <ListItemButton onClick={()=>router.push(item.url)}>
                                <ListItemIcon>
                                    {item.icon}
                                </ListItemIcon>
                                <ListItemText primary={item.title} />
                            </ListItemButton>
                        </ListItem>
                    ))
                }
                <ListItemButton onClick={handleClick}>
                    <ListItemIcon>
                        <SettingsIcon />
                    </ListItemIcon>
                    <ListItemText primary="System Configuration" />
                    {open ? <ExpandLess /> : <ExpandMore />}
                </ListItemButton>
                <Collapse in={open} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        {
                            SystemConfContent.map((item, index) => (
                                <ListItemButton key={index} onClick={()=>router.push(item.url)} sx={{ pl: 4 }}>
                                    <ListItemIcon>
                                        {item.icon}
                                    </ListItemIcon>
                                    <ListItemText primary={item.title} />
                                </ListItemButton>
                            ))
                        }
                    </List>
                </Collapse>
            </List>
            <List sx={{ mt: 'auto' }}>
                <ListItem>
                    <ListItemButton onClick={handleLogout}>
                        <ListItemIcon>
                            <LogoutIcon />
                        </ListItemIcon>
                        <ListItemText primary="Logout" />
                    </ListItemButton>
                </ListItem>
            </List>
        </Drawer>
        <Box
            component="main"
            sx={{ flexGrow: 1, bgcolor: '#DED0D0', py: 10, px: 2, minHeight: '100vh' }}
        >
           { children } 
        </Box>
    </Box>
  );
}

export default Layout;