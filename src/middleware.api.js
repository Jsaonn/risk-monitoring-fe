import { NextResponse } from "next/server";

export default function middleware(req) {
    let verify = req.cookies.get('token')
    let url = req.url

    if (!verify && url.includes('/dashboard')) {
        return NextResponse.redirect("http://localhost:3000")
    }

    if (!verify && url.includes('/system-config')) {
        return NextResponse.redirect("http://localhost:3000")
    }

    if (verify && url === "http://localhost:3000/login") {
        return NextResponse.redirect("http://localhost:3000/dashboard")
    }

    if (verify && url === "http://localhost:3000/register") {
        return NextResponse.redirect("http://localhost:3000/dashboard")
    }

}