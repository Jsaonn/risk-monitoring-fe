import '../styles/globals.css'
import Layout from '@layouts/Layout'
import Cookies from 'js-cookie'

function MyApp({ Component, pageProps }) {

  return (
    <>
     {Cookies.get('token') ? (
        <Layout>
          <Component {...pageProps} />
        </Layout>
      ) : (
        <Component {...pageProps} />
      )
      }
    </>
  )
}

export default MyApp
