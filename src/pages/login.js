import Cookies from "js-cookie"
import { useRouter } from "next/router"
import { useState } from "react"
import { 
    Box,
    Button,
    Card,
    Grid,
    TextField,
    Typography,
    FormControl,
    InputLabel,
    OutlinedInput,
    InputAdornment,
    IconButton,
    Link
} from '@mui/material';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { apiLogin } from "lib/axios";

export default function Login() {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [showPassword, setShowPassword] = useState(false);
    const router = useRouter()

    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const handleEmail = (event) => {
        let value = event.target.value
        setEmail(value)
    }

    const handlePassword = (event) => {
        let value = event.target.value
        setPassword(value)
    }

    const handleLogin = (event) => {
        if (!email || !password) {
            alert('Terdapat field yang belum diisi!')
            return
        }

        event.preventDefault()
        const data = {
            "email": email,
            "password": password
        }

        apiLogin(data).then(result => {
            Cookies.set('token', result.data.jwt, {expires: 1})
            Cookies.set('email', result.data.email, {expires: 1})
            Cookies.set('role', result.data.role, {expires: 1})
            router.push('/dashboard')
        }).catch(() => {
            alert('Invalid Credentials!')
        })
    }

    return(
        <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', minHeight: '100vh', background: '#DED0D0' }}>
            <Card sx={{ p: 5, width: '500px', height: '500px', background: '#fff' }}>
                <Typography variant="h3" align="center">Login Form</Typography>
                <Grid container rowSpacing={2} columnSpacing={{ md: 6 }} sx={{ my: 1 }}>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            name="email"
                            label="Email"
                            variant="outlined"
                            required
                            placeholder="user@aaa.com"
                            onChange={handleEmail}
                            value={email}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <FormControl sx={{ width: '100%' }} variant="outlined" required>
                            <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={showPassword ? 'text' : 'password'}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={handleMouseDownPassword}
                                            edge="end"
                                        >
                                        {showPassword ? <VisibilityOff /> : <Visibility />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                label="Password"
                                onChange={handlePassword}
                                value={password}
                            />
                        </FormControl>
                    </Grid>
                </Grid>
                <Button variant="contained" color="primary" fullWidth onClick={handleLogin} sx={{ mt: 3, py: 2 }}>Log in</Button>
                <Box sx={{ display: 'flex', gap: 1, alignItems: 'center', justifyContent: 'center', mt: 3 }}>
                    <Typography variant="body2">Tidak punya akun?</Typography>
                    <Link href="/register" variant="body2">Buat disini.</Link>
                </Box>
            </Card>
        </Box>
    )
}