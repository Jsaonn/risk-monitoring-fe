import Cookies from "js-cookie"
import { useRouter } from "next/router"
import { useState, useEffect } from "react"
import { 
    Box,
    Button,
    Card,
    Grid,
    TextField,
    Typography,
    FormControl,
    InputLabel,
    OutlinedInput,
    InputAdornment,
    IconButton,
    Link,
    Select,
    MenuItem
} from '@mui/material';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { apiRegister, apiGetAllRolesData } from "lib/axios";

export default function Register() {
    const [rolesOption, setRolesOption] = useState([])
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [passwordConfirm, setPasswordConfirm] = useState("")
    const [role, setRole] = useState("admin")
    const [showPassword, setShowPassword] = useState(false);
    const router = useRouter()

    useEffect(() => {
        const getRolesData = async () => {
            apiGetAllRolesData().then(result => {
                setRolesOption(result.data.data)
            })
        }

        if (!rolesOption.length) {
            getRolesData()
        }

    }, [rolesOption])

    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const handleName = (event) => {
        let value = event.target.value
        setName(value)
    }

    const handleEmail = (event) => {
        let value = event.target.value
        setEmail(value)
    }

    const handlePassword = (event) => {
        let value = event.target.value
        setPassword(value)
    }

    const handlePasswordConfirm = (event) => {
        let value = event.target.value
        setPasswordConfirm(value)
    }

    const handleRole = (event) => {
        let value = event.target.value
        setRole(value)
    }

    const handleRegister = (event) => {
        if (!name || !email || !password || !passwordConfirm) {
            alert('Terdapat field yang belum diisi!')
            return
        }

        if (password !== passwordConfirm) {
            alert('Password dan konfirmasi password berbeda!')
            return
        }

        event.preventDefault()
        const data = {
            "name": name,
            "email": email,
            "password": password,
            "role": role
        }

        apiRegister(data).then(result => {
            console.log(result.data);
            router.push('/login')
        }).catch((err) => {
            alert(err.message)
        })
    }

    return(
        <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', minHeight: '100vh', background: '#DED0D0' }}>
            <Card sx={{ p: 5, width: '500px', height: '600px', background: '#fff' }}>
                <Typography variant="h3" align="center">Register Form</Typography>
                <Grid container rowSpacing={2} columnSpacing={{ md: 6 }} sx={{ my: 1 }}>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            name="name"
                            label="Nama"
                            variant="outlined"
                            required
                            placeholder="John Doe"
                            onChange={handleName}
                            value={name}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            name="email"
                            label="Email"
                            variant="outlined"
                            required
                            placeholder="user@aaa.com"
                            onChange={handleEmail}
                            value={email}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <FormControl sx={{ width: '100%' }} variant="outlined" required>
                            <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={showPassword ? 'text' : 'password'}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={handleMouseDownPassword}
                                            edge="end"
                                        >
                                        {showPassword ? <VisibilityOff /> : <Visibility />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                label="Password"
                                onChange={handlePassword}
                                value={password}
                            />
                        </FormControl>
                    </Grid>

                    <Grid item xs={12}>
                        <FormControl sx={{ width: '100%' }} variant="outlined" required>
                            <InputLabel htmlFor="outlined-adornment-password">Konfirmasi Password</InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={showPassword ? 'text' : 'password'}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={handleMouseDownPassword}
                                            edge="end"
                                        >
                                        {showPassword ? <VisibilityOff /> : <Visibility />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                label="Password"
                                onChange={handlePasswordConfirm}
                                value={passwordConfirm}
                            />
                        </FormControl>
                    </Grid>
                </Grid>

                <Grid item xs={12}>
                        <FormControl fullWidth required>
                            <InputLabel id="select-class-label">Roles</InputLabel>
                            <Select 
                                fullWidth 
                                labelId="select-class-label"
                                id="select-class"
                                value={role}
                                onChange={handleRole}
                                input={<OutlinedInput label="Roles" />}
                            >
                                {rolesOption && rolesOption.map((role) => (
                                    <MenuItem
                                        key={role.id}
                                        value={role.name}
                                    >
                                        {role.name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                <Button variant="contained" color="primary" fullWidth onClick={handleRegister} sx={{ mt: 3, py: 2 }}>Register</Button>
                <Box sx={{ display: 'flex', gap: 1, alignItems: 'center', justifyContent: 'center', mt: 3 }}>
                    <Typography variant="body2">Sudah punya akun?</Typography>
                    <Link href="/login" variant="body2">Masuk disini.</Link>
                </Box>
            </Card>
        </Box>
    )
}